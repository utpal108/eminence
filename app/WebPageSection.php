<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class WebPageSection extends Model
{
    protected $table='ic_web_page_sections';
    protected $casts=['contents'=>'array'];
    protected $fillable=['name','contents'];
}
