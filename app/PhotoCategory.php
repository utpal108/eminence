<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PhotoCategory extends Model
{
    protected $table='ic_photo_categories';
    protected $fillable=['name','category_class'];

    public function photos(){
    	return $this->hasMany('App\PhotoGallery','category_id');
    }
}
