<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class LatestNews extends Model
{
    protected $table='ic_latest_news';
    protected $fillable=['title','details','news_image','user_id'];
    public function user(){
    	return $this->belongsTo('App\User');
    }
}
