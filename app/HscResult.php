<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class HscResult extends Model
{
    protected $table='ic_hsc_result';
    protected $fillable=['year','download_url','about'];
}
