<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class StudyGroup extends Model
{
    protected $table='ic_study_groups';
    protected $fillable=['group_name','about_group','department_id'];

    public function achievements(){
    	return $this->hasMany('App\CollegeAchievements','group_id');
    }
}
