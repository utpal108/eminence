<?php

namespace App\Http\Controllers;

use App\ExamType;
use Illuminate\Http\Request;

class ExamTypesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $exam_types=ExamType::paginate(15);
        return view('admin.exam.exam_type.index',['exam_types'=>$exam_types]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.exam.exam_type.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        ExamType::create($request->all());
        return redirect()->action('ExamTypesController@index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
    	$exam_type=ExamType::find($id);
	    return view('admin.exam.exam_type.edit',['exam_type'=>$exam_type]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $exam_type=ExamType::find($id);
	    $exam_type->exam_type=$request->exam_type;
	    $exam_type->about=$request->about;
	    $exam_type->save();
        return redirect()->back();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        ExamType::destroy($id);
        return redirect()->action('ExamTypesController@index');
    }
}
