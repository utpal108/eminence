<?php

namespace App\Http\Controllers;

use App\Subject;
use App\User;
use Illuminate\Support\Facades\File;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Storage;
use Spatie\Permission\Models\Role;


class UserController extends Controller
{
	/**
	 * Display a listing of the resource.
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function index()
	{
		$all_types=User::distinct()->where('type','<>','super_admin')->get(['type']);
		$users=User::where('type','<>','super_admin')->orderBy('user_order','ASC')->get();
		return view('admin.users.index',['users'=>$users,'all_types'=>$all_types]);
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function create()
	{
		$roles=Role::all();
		return view('admin.users.create', compact('roles'));
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @param  \Illuminate\Http\Request  $request
	 * @return \Illuminate\Http\Response
	 */
	public function store(Request $request)
	{
		if ($request->email != ''){
			$this->validate($request, [
				'email' => 'unique:ic_users|max:255',
			]);
		}

		$allData=$request->all();
		if($request->hasFile('profile_image')){
			$allData['profile_image']=$request->file('profile_image')->store('images');
		}
		$allData['password']=bcrypt($request->password);
		$user=User::create($allData);

		$user->assignRole($request->roles);
		return redirect()->action('UserController@index');
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return \Illuminate\Http\Response
	 */
	public function show($id)
	{
		//
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return \Illuminate\Http\Response
	 */
	public function edit($id)
	{
		$user=User::find($id);
		$user_roles=$user->roles()->pluck('name')->toArray();
		$roles=Role::all();
		return view('admin.users.edit', compact('user', 'user_roles', 'roles'));
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  \Illuminate\Http\Request  $request
	 * @param  int  $id
	 * @return \Illuminate\Http\Response
	 */
	public function update(Request $request, $id)
	{
		$user=User::find($id);
		$user->syncRoles($request->roles);
		if ($request->email != ''){
			$this->validate($request,[
				'email'=>'email|unique:ic_users,email,'.$user->id,
			]);
		}

		if ($request->hasFile('profile_image')){
			if (Storage::exists($user->profile_image)){
				Storage::delete($user->profile_image);
			}
			$user->profile_image=$request->file('profile_image')->store('images');
		}
		$user->name=$request->name;
		$user->email=$request->email;
		$user->qualification=$request->qualification;
		$user->designation=$request->designation;
		$user->type=$request->type;
		$user->status=$request->status;
		$user->is_admin=$request->is_admin;
		$user->biography=$request->biography;

		if ($request->password != ''){
			$user->password =bcrypt($request->password);
		}
		$user->save();

		return redirect()->back();
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return \Illuminate\Http\Response
	 */
	public function destroy($id)
	{
		$user=User::find($id);
		if (Storage::exists($user->profile_image)){
			Storage::delete($user->profile_image);
		}
		User::destroy($id);
		return redirect()->action('UserController@index');
	}

	public function authenticate(Request $request){
		if (Auth::attempt(['email'=>$request->email,'password'=>$request->password, 'status'=>1, 'is_admin'=>1])){
//			return redirect()->action('UserController@index');
            return redirect('admin/dashboard');
		}
		else{
			return redirect()->back()->with('error', '* You Are Not Permitted');
		}
	}

	public function logout(){
		Auth::logout();
		return redirect()->action('AdminController@index');
	}

	public function order_user(Request $request){
		$user_type=$request->user_type;
		if ($user_type == 'all'){
			$all_users = User::orderBy('user_order','ASC')->get();
			$itemID = $request->itemID;
			$itemIndex = $request->itemIndex;

			foreach($all_users as $user){
				return User::where('id','=',$itemID)->update(array('user_order'=> $itemIndex));
			}
		}
		else{
			$all_users = User::where('type',$user_type)->orderBy('user_type_order','ASC')->get();
			$itemID = $request->itemID;
			$itemIndex = $request->itemIndex;

			foreach($all_users as $user){
				return User::where('id','=',$itemID)->update(array('user_type_order'=> $itemIndex));
			}
		}
	}

	public function order_user_type(Request $request){
		$user_type=$request->user_type;
		if ($user_type == 'all'){
			$all_users = User::orderBy('user_order','ASC')->get();
			$itemID = $request->itemID;
			$itemIndex = $request->itemIndex;

			foreach($all_users as $user){
				return User::where('id','=',$itemID)->update(array('user_order'=> $itemIndex));
			}
		}
		else{
			$all_users = User::where('type',$user_type)->orderBy('user_type_order','ASC')->get();
			$itemID = $request->itemID;
			$itemIndex = $request->itemIndex;

			foreach($all_users as $user){
				return User::where('id','=',$itemID)->update(array('user_type_order'=> $itemIndex));
			}
		}

	}

	public function assign_subject(){
	    $teachers=User::where('type','faculty_member')->paginate(15);
	    return view('admin.users.assign_subject.index',compact('teachers'));
    }

    public function edit_assign_subject($id){
        $subjects=Subject::with('department')->orderBy('department_id')->get();
        $teacher=User::find($id);
        return view('admin.users.assign_subject.edit',compact('subjects','teacher'));
    }

    public function update_assign_subject(Request $request, $id){
	    $teacher=User::find($id);
	    $teacher->assign_subjects=$request->assign_subjects;
	    $teacher->save();
	    flash('Subject Assign Successfully')->important();
	    return redirect()->back();
    }
}
