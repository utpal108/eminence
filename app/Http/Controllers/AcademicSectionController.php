<?php

namespace App\Http\Controllers;

use App\AcademicSection;
use App\Department;
use App\StudyGroup;
use Illuminate\Http\Request;

class AcademicSectionController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $all_sections=AcademicSection::paginate(15);
        return view('admin.academic_section.index',['all_sections'=>$all_sections]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
	    $all_departments=Department::all();
        return view('admin.academic_section.create',['all_departments'=>$all_departments]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        AcademicSection::create($request->all());
        return redirect()->action('AcademicSectionController@index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
    	$all_departments=Department::all();
        $section=AcademicSection::find($id);
        $all_groups=StudyGroup::where('department_id',$section->department_id)->get();
        return view('admin.academic_section.edit',['section'=>$section,'all_departments'=>$all_departments,'all_groups'=>$all_groups]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $section=AcademicSection::find($id);
        $section->section_name=$request->section_name;
        $section->about_section=$request->about_section;
        $section->department_id=$request->department_id;
        $section->group_id=$request->group_id;
        $section->save();

        return redirect()->back();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        AcademicSection::destroy($id);
        return redirect()->action('AcademicSectionController@index');
    }
}
