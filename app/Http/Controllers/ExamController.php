<?php

namespace App\Http\Controllers;

use App\AcademicSection;
use App\Department;
use App\Exam;
use App\ExamCategory;
use App\ExamType;
use App\StudyGroup;
use App\Subject;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class ExamController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
		$all_exams=Exam::with(['exam_type','exam_category','department','group','section','subject'])->paginate(15);
		return view('admin.exam.index',['all_exams'=>$all_exams]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $departments=Department::all();
        $exam_types=ExamType::all();
        $exam_categories=ExamCategory::all();
        $all_exam_categories='';
        foreach ($exam_categories as $exam_category){
	        $all_exam_categories=$all_exam_categories.'<option value="'.$exam_category->id.'">'.$exam_category->exam_name.'</option>';
        }

        return view('admin.exam.create',['departments'=>$departments,'exam_types'=>$exam_types, 'exam_categories'=>$all_exam_categories]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
    	$all_exams=$request->exam_name;
    	$pass_marks=$request->pass_mark;
    	for ($i=0; $i<count($all_exams); $i++){
		    Exam::firstOrCreate(['exam_category_id' => $all_exams[$i], 'exam_type_id' => $request->exam_type_id, 'department_id' => $request->department_id, 'group_id'=>$request->group_id, 'section_id'=>$request->section_id, 'subject_id'=>$request->subject_id, 'pass_mark'=>$pass_marks[$i]]);
	    }
	    return redirect()->action('ExamController@index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
    	$exam=Exam::find($id);
        $departments=Department::all();
    	$groups=StudyGroup::where('department_id',$exam->department_id)->get();
    	$sections=AcademicSection::where([['department_id',$exam->department_id],['group_id',$exam->group_id]])->get();
    	$subjects=Subject::where([['department_id',$exam->department_id],['group_id',$exam->group_id],['section_id',$exam->section_id]])->get();
    	$exam_types=ExamType::all();
    	$exam_categories=ExamCategory::all();


    	return view('admin.exam.edit',['exam'=>$exam,'exam_types'=>$exam_types,'exam_categories'=>$exam_categories ,'departments'=>$departments, 'groups'=>$groups, 'sections'=>$sections, 'subjects'=>$subjects]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
	    if (Exam::where([['id','<>',$id], ['exam_category_id', $request->exam_category_id],['exam_type_id', $request->exam_type_id],['department_id',$request->department_id],['group_id',$request->group_id],['section_id',$request->section_id],['subject_id',$request->subject_id]])->exists()){
		    return redirect()->back()->withErrors('Exam Name Already Exist')->withInput();
	    }
	    else{
		    $exam=Exam::find($id);
		    $exam->exam_type_id=$request->exam_type_id;
		    $exam->exam_category_id=$request->exam_category_id;
		    $exam->department_id=$request->department_id;
		    $exam->group_id=$request->group_id;
		    $exam->section_id=$request->section_id;
		    $exam->subject_id=$request->subject_id;
		    $exam->pass_mark=$request->pass_mark;
		    $exam->save();

		    session()->flash('success','Exam Updated Successfully');

		    return redirect()->back();
	    }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Exam::destroy($id);
        return redirect()->action('ExamController@index');
    }
}
