<?php

namespace App\Http\Controllers;

use App\NoticeBoard;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Storage;

class NoticeBoardController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
    	$all_notice=NoticeBoard::with('user')->orderBy('notice_order','ASC')->get();
        return view('admin.notice_board.index',['all_notice'=>$all_notice]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.notice_board.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
    	$allData=$request->all();
    	$allData['user_id']=Auth::id();
    	if ($request->hasFile('notice_image')){
    		$allData['notice_image']=$request->file('notice_image')->store('images');
	    }
        NoticeBoard::create($allData);

        return redirect()->action('NoticeBoardController@index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($title)
    {
    	$title=str_replace('_',' ',$title);
        $notice=NoticeBoard::where('title',$title)->first();
        return view('pages.show_notice',['notice'=>$notice]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $notice=NoticeBoard::find($id);
        return view('admin.notice_board.edit',['notice'=>$notice]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $notice=NoticeBoard::find($id);
        $notice->title=$request->title;
        $notice->details=$request->details;
        if ($request->hasFile('notice_image')){
        	if (Storage::exists(Storage::url($notice->notice_image))){
		        Storage::delete(Storage::url($notice->notice_image));
	        }
	        $notice->notice_image=$request->file('notice_image')->store('images');
        }
        $notice->save();

        return redirect()->back();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
    	$notice=NoticeBoard::find($id);
    	if (Storage::exists($notice->notice_image)){
    		Storage::delete($notice->notice_image);
	    }
        NoticeBoard::destroy($id);
        return redirect()->action('NoticeBoardController@index');
    }

	public function order_notice(Request $request){
		$all_notice = NoticeBoard::orderBy('notice_order','ASC')->get();
		$itemID = $request->itemID;
		$itemIndex = $request->itemIndex;

		foreach($all_notice as $notice){
			return NoticeBoard::where('id','=',$itemID)->update(array('notice_order'=> $itemIndex));
		}
	}
}
