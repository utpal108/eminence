<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Slider;
use Illuminate\Support\Facades\Storage;

class AdminSliderController extends Controller
{
	/**
	 * Display a listing of the resource.
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function index()
	{
		$sliders=Slider::paginate(15);
		return view('admin.slider.index',['sliders'=>$sliders]);
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function create()
	{
		return view('admin.slider.create');
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @param  \Illuminate\Http\Request  $request
	 * @return \Illuminate\Http\Response
	 */
	public function store(Request $request)
	{
		$allData=$request->all();
		if ($request->hasFile('bg_image')){
			$allData['bg_image'] = $request->file('bg_image')->store('images/slider');
		}

		Slider::create($allData);
		return redirect()->action('AdminSliderController@index');
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return \Illuminate\Http\Response
	 */
	public function show($id)
	{
		//
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return \Illuminate\Http\Response
	 */
	public function edit(Slider $slider)
	{
		return view('admin.slider.edit',['slider'=>$slider]);
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  \Illuminate\Http\Request  $request
	 * @param  int  $id
	 * @return \Illuminate\Http\Response
	 */
	public function update(Request $request, Slider $slider)
	{
		$slider->title=$request->title;
		$slider->subtitle=$request->subtitle;
		if ($request->hasFile('bg_image')){
			if (Storage::exists($slider->bg_image)){
				Storage::delete($slider->bg_image);
			}
			$slider->bg_image = $request->file('bg_image')->store('images/slider');
		}

		$slider->save();
		return redirect()->back();
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return \Illuminate\Http\Response
	 */
	public function destroy($id)
	{
		$slider=Slider::find($id);
		if (Storage::exists($slider->bg_image)){
			Storage::delete($slider->bg_image);
		}
		Slider::destroy($id);
		return redirect()->action('AdminSliderController@index');
	}
}
