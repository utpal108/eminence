<?php

namespace App\Http\Controllers;

use App\Department;
use App\Student;
use App\User;
use Illuminate\Http\Request;

class DashboardController extends Controller
{
    public function index(){
        $users=User::all()->count();
        $students=Student::all()->count();
        $faculty_members=User::where('type','faculty_member')->get()->count();
        $departments=Department::all()->count();

        return view('admin.index',compact('users','students','faculty_members','departments'));
    }
}
