<?php

namespace App\Http\Controllers;

use App\AcademicSection;
use App\Department;
use App\MainSubject;
use App\Subject;
use Illuminate\Http\Request;
use App\StudyGroup;
use Illuminate\Support\Facades\Auth;

class SubjectController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $all_subjects=Subject::with('department','group','section')->orderBy('subject_order','ASC')->get();
        return view('admin.academic_subject.index',['all_subjects'=>$all_subjects]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
    	$all_departments=Department::all();
	    $main_subjects=MainSubject::all();
        return view('admin.academic_subject.create',['all_departments'=>$all_departments, 'main_subjects'=>$main_subjects]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        Subject::create($request->all());
        return redirect()->action('SubjectController@index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
    	$subject=Subject::find($id);
    	$all_departments=Department::all();
    	$all_groups=StudyGroup::where('department_id',$subject->department_id)->get();
    	$all_sections=AcademicSection::where([['department_id',$subject->department_id],['group_id',$subject->group_id]])->get();
	    $main_subjects=MainSubject::all();
        return view('admin.academic_subject.edit',['subject'=>$subject,'all_groups'=>$all_groups, 'all_departments'=>$all_departments, 'all_sections'=>$all_sections, 'main_subjects'=>$main_subjects]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $subject=Subject::find($id);
        $subject->department_id=$request->department_id;
        $subject->group_id=$request->group_id;
        $subject->section_id=$request->section_id;
        $subject->core_subject_id=$request->core_subject_id;
        $subject->subject_code=$request->subject_code;
        $subject->subject_name=$request->subject_name;
        $subject->save();
        return redirect()->back();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Subject::destroy($id);
        return redirect()->action('SubjectController@index');
    }

    public function group_wise_data(Request $request){
		$subjects=Subject::where([['department_id',$request->department_id],['group_id',$request->group_id]])->get();
		$all_sections=AcademicSection::where([['department_id',$request->department_id],['group_id',$request->group_id]])->get();
		if ($subjects === null){
			$response['subjects']='';
		}
		else{
			$data='';
			foreach ($subjects as $subject){
				$data=$data.'<option value="'.$subject->id .'">'. $subject->subject_name .'</option>';
			}
			$response['subjects']=$data;
		}

	    if ($all_sections === null){
		    $response['sections']='';
	    }
	    else{
		    $data='<option value='.''.'>'.'-- Select Section --'.'</option>';
		    foreach ($all_sections as $section){
			    $data=$data.'<option value="'.$section->id .'">'. $section->section_name .'</option>';
		    }
		    $response['sections']=$data;
	    }

		echo json_encode($response);
    }

    public function section_wise_data(Request $request){
        $user=Auth::user();
        $subjects=Subject::where([['department_id',$request->department_id],['group_id',$request->group_id],['section_id',$request->section_id]])->get();
        if ($subjects === null){
            $response['subjects']='';
        }
        else{
            $data='';
            foreach ($subjects as $subject){
                if (!is_null($user->assign_subjects) && in_array($subject->id, $user->assign_subjects)){
                    $data=$data.'<option value="'.$subject->id .'">'. $subject->subject_name .'</option>';
                }
            }
            $response['subjects']=$data;
        }

        echo json_encode($response);
    }

    public function department_wise_group(Request $request){
	    $all_groups=StudyGroup::where('department_id',$request->department_id)->get();
    	$group_data='';
    	foreach ($all_groups as $group){
		    $group_data=$group_data.'<option value="'.$group->id.'" >'.$group->group_name.'</option>';
	    }
	    $response['groups']=$group_data;

    	echo json_encode($response);
    }

    public function order_subjects(Request $request){
        parse_str($request->orders, $orders);

        foreach($orders['order'] as $order => $id){
            $subject=Subject::findOrFail($id);
            $subject->subject_order=$order;
            $subject->save();
        }
        return response()->json(['success'=>'Success']);
    }
}
