<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;

class DownloadFileController extends Controller
{
    public function download(Request $request){
    	if ($request->url != null && Storage::exists($request->url)){
		    return response()->download(storage_path("app/public/{$request->url}"));
	    }
    }
}
