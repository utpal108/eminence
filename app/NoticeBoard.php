<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class NoticeBoard extends Model
{
    protected $table='ic_notice_board';
    protected $fillable=['title','details','user_id','notice_image'];

    public function user(){
    	return $this->belongsTo('App\User');
    }
}
