<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ResultGrade extends Model
{
    protected $table='ic_result_grade';
    protected $fillable=['mark_range_from','mark_range_to','gpa','grade'];
}
