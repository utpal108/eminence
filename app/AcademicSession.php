<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class AcademicSession extends Model
{
    protected $table='ic_academic_session';
    protected $fillable=['session_name','session_details'];
}
