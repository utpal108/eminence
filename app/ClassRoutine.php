<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ClassRoutine extends Model
{
    protected $table='ic_class_routine';
    protected $fillable=['department_id','routine_title','download_url','about_routine'];
}
