<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Support\Facades\DB;

class CreateStudentsTable extends Migration
{
	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('ic_students', function (Blueprint $table) {
			$table->increments('id');
			$table->string('name');
			$table->text('student_info');
			$table->integer('department_id')->unsigned();
			$table->foreign('department_id')->references('id')->on('ic_departments')->onDelete('cascade');
			$table->integer('group_id')->unsigned();
			$table->foreign('group_id')->references('id')->on('ic_study_groups')->onDelete('cascade');
			$table->integer('section_id')->unsigned();
			$table->foreign('section_id')->references('id')->on('ic_sections')->onDelete('cascade');
			$table->integer('session_id')->unsigned();
			$table->string('student_id');
			$table->integer('fourth_subject')->unsigned();
			$table->foreign('fourth_subject')->references('id')->on('ic_core_subjects')->onDelete('cascade');
			$table->string('password')->nullable();
			$table->string('student_image');
			$table->timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		DB::statement('SET FOREIGN_KEY_CHECKS = 0');
		Schema::dropIfExists('ic_students');
		DB::statement('SET FOREIGN_KEY_CHECKS = 1');
	}
}
