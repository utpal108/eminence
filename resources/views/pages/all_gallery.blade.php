@extends('partials.main')

@section('content')
    @if(isset($content['header_status']) && $content['header_status'] == 'on')
        <!-- banner -->
        <section class="ic-banner gallery" style="background-image: url({{ Storage::url($content['header_image']) }})">
            <div class="container">
                <div class="row">
                    <div class="col-xs-12">
                        <div class="ic-banner-caption">
                            <h1>{{ $content['header_title'] }}</h1>
                            <div class="ic-breadcrumb">
                                <ul>
                                    <li><a href="/">Home</a></li>
                                    <li><span> > </span></li>
                                    <li>All gallery</li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <!-- banner /end -->
    @else
        <div class="container">
            <div class="row">
                <div class="col-xs-12">
                    <div class="ic-banner-caption ic-banner-caption-inverse">
                        <h1>{{ $content['header_title'] }}</h1>
                        <div class="ic-breadcrumb ic-breadcrumb-inverse">
                            <ul>
                                <li><a href="/">Home</a></li>
                                <li><span> > </span></li>
                                <li>All gallery</li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    @endif


    <!-- gallery -->
    @include('partials.all_gallery')
    <!-- gallery /end -->
@endsection

@section('js')
    <script>
        // Load more gallery images
        $(function () {
            var TotalElement = $(".single-gallery-col").length;
            var currentElement = 1;
            $(".single-gallery-col").slice(0, 1).show();

            $("#loadMore").on('click', function (e) {
                e.preventDefault();
                $(".single-gallery-col:hidden").slice(0, 1).fadeIn('600');
                currentElement=currentElement+1;
                if (TotalElement <= currentElement) {
                    $("#loadMore").css({"display":"none"});
                }
            });
        });
    </script>
@endsection