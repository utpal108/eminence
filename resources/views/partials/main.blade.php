
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Eminence</title>
    <link rel="shortcut icon" type="image/png" href="{{ Storage::url('images/favicon.png') }}"/>
    <link href="https://fonts.googleapis.com/css?family=Montserrat:300,400,400i,500,600,700,800,900%7CSource+Sans+Pro:300,400,400i,600,700,900" rel="stylesheet">
    <link href="{{ asset('css/bootstrap.min.css') }}" rel="stylesheet">
    <link href="{{ asset('css/font-awesome.min.css') }}" rel="stylesheet">
    <link href="{{ asset('css/slick.css') }}" rel="stylesheet">
    <link href="{{ asset('css/slicknav.min.css') }}" rel="stylesheet">
    <link href="{{ asset('css/jquery.mCustomScrollbar.css') }}" rel="stylesheet">
    <link href="{{ asset('css/jquery.fancybox.min.css') }}" rel="stylesheet">
    @yield('style')
    <link href="{{ asset('css/style.css') }}" rel="stylesheet">
    <link href="{{ asset('css/responsive.css') }}" rel="stylesheet">
    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]
      <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
</head>
<body>
<main>
    <!-- header -->
    @include('partials.header')
    <!-- header /end-->

    @yield('content')

    <!-- footer -->
    @include('partials.footer')
    <!-- footer /end -->
</main>
<script src="{{ asset('js/jquery.min.js') }}"></script>
<script src="{{ asset('js/bootstrap.min.js') }}"></script>
<script src="{{ asset('js/slick.min.js') }}"></script>
<script src="{{ asset('js/mixitup.min.js') }}"></script>
<script src="{{ asset('js/jquery.slicknav.min.js') }}"></script>
<script src="{{ asset('js/jquery.mCustomScrollbar.concat.min.js') }}"></script>
<script src="{{ asset('js/jquery.fancybox.js') }}"></script>
<script src="{{ asset('js/scripts.js') }}"></script>
<script>
    /*
* declare map as a global variable
*/
    var map;

    /*
     * use google maps api built-in mechanism to attach dom events
     */
    google.maps.event.addDomListener(window, "load", function () {

        /*
         * create map
         */
        var map = new google.maps.Map(document.getElementById("map_div"), {
            center: new google.maps.LatLng("{{ $footer['latitudinal'] }}", "{{ $footer['longitudinal'] }}"),
            zoom: 14,
            mapTypeId: google.maps.MapTypeId.ROADMAP
        });

        /*
         * create infowindow (which will be used by markers)
         */
        var infoWindow = new google.maps.InfoWindow();

        /*
         * marker creater function (acts as a closure for html parameter)
         */
        function createMarker(options, html) {
            var marker = new google.maps.Marker(options);
            if (html) {
                google.maps.event.addListener(marker, "click", function () {
                    infoWindow.setContent(html);
                    infoWindow.open(options.map, this);
                });
            }
            return marker;
        }

        /*
         * add markers to map
         */
        var marker0 = createMarker({
            position: new google.maps.LatLng("{{ $footer['latitudinal'] }}", "{{ $footer['longitudinal'] }}"),
            map: map,
            icon: "http://1.bp.blogspot.com/_GZzKwf6g1o8/S6xwK6CSghI/AAAAAAAAA98/_iA3r4Ehclk/s1600/marker-green.png"
        }, "<p>Eminence College</p>");
    });
</script>
@yield('js')
</body>
</html>