<div class="ic-nav">
    <div class="container">
        <div class="row">
            <div class="col-xs-12">
                <nav>
                    <ul id="menu">
                        <li class="@if(isset(request()->route()->parameters['slug']) && request()->route()->parameters['slug']=='/'){{ 'active' }} @endif"><a href="{{ route('page.show',['slag'=>'/']) }}">Home</a></li>
                        <li class="ic-dropdown @if(isset(request()->route()->parameters['slug']) && (request()->route()->parameters['slug']=='at_a_glance' || request()->route()->parameters['slug']=='board_management') ){{ 'active' }} @endif"><a href="#">About us</a>
                            <ul>
                                <li><a href="{{ route('page.show',['slag'=>'at_a_glance']) }}">At a glance</a></li>
                                <li><a href="{{ route('page.show',['slag'=>'board_management']) }}">Board of management</a></li>
                            </ul>
                        </li>
                        <li class="ic-dropdown @if(isset(request()->route()->parameters['slug']) && in_array(request()->route()->parameters['slug'],['faculty_members','rules_n_regulation','examination_system','administrative_staff','office_staff'])==true ){{ 'active' }} @endif"><a href="#">Academic</a>
                            <ul>
                                <li><a href="{{ route('page.show',['slag'=>'faculty_members']) }}">Faculty members</a></li>
                                <li><a href="{{ route('page.show',['slag'=>'rules_n_regulation']) }}">Rules &amp; regulation</a></li>
                                <li><a href="{{ route('page.show',['slag'=>'examination_system']) }}">Examination system</a></li>
                                <li class="ic-dropside"><a href="#">Office staff</a>
                                    <ul>
                                        <li><a href="{{ route('page.show',['slag'=>'administrative_staff']) }}">Adminstrative Staff</a></li>
                                        <li><a href="{{ route('page.show',['slag'=>'office_staff']) }}">Office Staff</a></li>
                                    </ul>
                                </li>
                            </ul>
                        </li>
                        <li class="ic-dropdown @if(isset(request()->route()->parameters['slug']) && in_array(request()->route()->parameters['slug'],['bba_course_outline','bba_entry_requirement','bba_fee_structure','bba_class_routine','cse_course_outline','cse_entry_requirement','cse_fee_structure','cse_class_routine','hsc_course_outline','hsc_entry_requirement','hsc_fee_structure','hsc_class_routine'])==true ){{ 'active' }} @endif"><a href="#">Department</a>
                            <ul>
                                <li class="ic-dropside"><a href="#">BBA</a>
                                    <ul>
                                        <li><a href="{{ route('page.show',['slag'=>'bba_course_outline']) }}">Course outline</a></li>
                                        <li><a href="{{ route('page.show',['slag'=>'bba_entry_requirement']) }}">Entry requirement</a></li>
                                        <li><a href="{{ route('page.show',['slag'=>'bba_fee_structure']) }}">Fees structure</a></li>
                                        <li><a href="{{ route('page.show',['slag'=>'bba_class_routine']) }}">Class routine</a></li>
                                    </ul>
                                </li>
                                <li class="ic-dropside"><a href="#">CSE</a>
                                    <ul>
                                        <li><a href="{{ route('page.show',['slag'=>'cse_course_outline']) }}">Course outline</a></li>
                                        <li><a href="{{ route('page.show',['slag'=>'cse_entry_requirement']) }}">Entry requirement</a></li>
                                        <li><a href="{{ route('page.show',['slag'=>'cse_fee_structure']) }}">Fees structure</a></li>
                                        <li><a href="{{ route('page.show',['slag'=>'cse_class_routine']) }}">Class routine</a></li>
                                    </ul>
                                </li>
                                <li class="ic-dropside"><a href="#">HSC</a>
                                    <ul>
                                        <li><a href="{{ route('page.show',['slag'=>'hsc_course_outline']) }}">Course outline</a></li>
                                        <li><a href="{{ route('page.show',['slag'=>'hsc_entry_requirement']) }}">Entry requirement</a></li>
                                        <li><a href="{{ route('page.show',['slag'=>'hsc_fee_structure']) }}">Fees structure</a></li>
                                        <li><a href="{{ route('page.show',['slag'=>'hsc_class_routine']) }}">Class routine</a></li>
                                    </ul>
                                </li>
                            </ul>
                        </li>
                        <li class="@if(isset(request()->route()->parameters['slug']) && request()->route()->parameters['slug']=='college_achievements'){{ 'active' }} @endif"><a href="{{ route('page.show',['slag'=>'college_achievements']) }}">College Achivements</a></li>
                        <li class="@if(isset(request()->route()->parameters['slug']) && request()->route()->parameters['slug']=='results'){{ 'active' }} @endif"><a href="{{ route('page.show',['slag'=>'results']) }}">Results</a></li>
                        <li class="@if(isset(request()->route()->parameters['slug']) && request()->route()->parameters['slug']=='all_gallery'){{ 'active' }} @endif"><a href="{{ route('page.show',['slag'=>'all_gallery']) }}">All gallery</a></li>
                        <li class="@if(isset(request()->route()->parameters['slug']) && request()->route()->parameters['slug']=='admission'){{ 'active' }} @endif"><a href="{{ route('page.show',['slag'=>'admission']) }}">Admission</a></li>
                        <li class="@if(isset(request()->route()->parameters['slug']) && request()->route()->parameters['slug']=='contact_us'){{ 'active' }} @endif"><a href="{{ route('page.show',['slag'=>'contact_us']) }}">Contact</a></li>
                        <li class="ic-dropdown"><a href="#">Eminence Online</a>
                            <ul>
                                <li class="text-center"><a href="{{ action('AdminController@index') }}">Admin</a></li>
                            </ul>
                        </li>
                    </ul>
                </nav>
            </div>
        </div>
    </div>
</div>