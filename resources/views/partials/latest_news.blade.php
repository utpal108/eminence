<div class="ic-ln-col">
    <ul>
        @foreach($all_news as $news)
            <li>
                <a href="{{ action('LatestNewsController@show',['title'=>strtolower(str_replace(' ','_',$news->title))]) }}">{{ $news->title }}<br>{{ $news->created_at->format('d M Y') }}</a>
            </li>
        @endforeach
    </ul>
</div>