<div class="col-md-8">

    @foreach($all_messages as $message)
        <div class="ic-single-message">
            <div class="ic-title">
                <h2>{{ $message->message_title }}</h2>
            </div>
            <div class="row">
                <div class="col-xs-5 col-xxs-12">
                    <img class="img-responsive" src="{{ Storage::url($message->profile_image) }}" alt="" style="max-height: 360px">
                </div>
                <div class="col-xs-7 col-xxs-12">
                    <div class="ic-message">
                        <span>{{ $message->designation }}</span>
                        <span>{{ $message->provide_by }}, {{ $message->qualification }}</span>
                        {!! str_limit($message->message, 300, '(...)') !!}<br>
                        <a class="ic-link" href="{{ action('MessageForUniversityController@show',['title'=>strtolower(str_replace(' ','-',$message->message_title))]) }}">Read more</a>
                    </div>
                </div>
            </div>
        </div>
    @endforeach
</div>