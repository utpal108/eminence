<section class="ic-other-staff">
    <div class="container">
        <div class="ic-flex">
            <h2 class="hidden">hidden title</h2>
            @foreach($all_office_staff as $staff)
                <div class="ic-card">
                    <div class="ic-card-fig">
                        <img class="img-responsive" style="width: 263px; height: 292px" src="@if($staff->profile_image != ''){{ Storage::url($staff->profile_image) }} @else{{ Storage::url('images/demo_user.png') }} @endif" alt="">
                        <div class="ic-card-caption">
                            <span>{{ $staff->name }}</span>
                            @if($staff->qualification != '' )
                                <span>{{ $staff->qualification }}</span>
                            @endif
                            <span>{{ $staff->designation }}</span>
                        </div>
                    </div>
                </div>
            @endforeach
        </div>
    </div>
</section>