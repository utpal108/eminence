<section class="ic-governance">
    <div class="container">
        <div class="ic-flex">
            <h2 class="hidden">Hidden title</h2>
            @foreach($board_members as $board_member)
                <div class="ic-card">
                    <div class="ic-card-fig">
                        <img class="img-responsive" style="width: 263px; height: 292px" src="@if($board_member->profile_image != ''){{  Storage::url($board_member->profile_image) }} @else{{ Storage::url('images/demo_user.png') }} @endif" alt="">
                        <div class="ic-card-caption">
                            <span>{{ $board_member->name }}</span>
                            <span>{!! $board_member->designation !!}</span>
                        </div>
                    </div>
                </div>
            @endforeach
        </div>
    </div>
</section>