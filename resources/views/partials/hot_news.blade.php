
@if($all_hot_news != '')
    <!-- hot news -->
    <section class="ic-ltn">
        <div class="container">
            <div class="row">
                <div class="ic-ltn-col title">
                    <h2>Latest News</h2>
                </div>
                <div class="ic-ltn-col ic-marque">
                    <marquee direction="left" scrollamount="10">
                        {{ $all_hot_news }}
                    </marquee>
                </div>
            </div>
        </div>
    </section>
    <!-- hot news /end -->
@endif