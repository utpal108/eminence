@extends('admin.partials.main')

@section('content')
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <h1>
                Add New Course Outline
            </h1>
            @if(session()->has('error'))
                <br>
                <span class="text-danger">{{ session('error') }}</span>
            @endif
            <ol class="breadcrumb">
                <li><a href="#"><i class="fa fa-dashboard"></i> Dashboard</a></li>
                <li><a href="#">Course Outline</a></li>
                <li class="active">Add New Course Outline</li>
            </ol>
        </section>

        <!-- Main content -->
        <section class="content">
            <div class="row">
                {!! Form::open(['action'=>'CourseOutlineController@store','files'=>true]) !!}
                    <div class="col-xs-9">
                        <div class="box">
                            <!-- form start -->
                            <div class="box-body">
                                <div class="form-group">
                                    <label for="department_id">Department Name</label>
                                    <select name="department_id" id="department_id" class="form-control select2 select2-hidden-accessible" tabindex="-1" aria-hidden="true">
                                        @foreach($all_departments as $department)
                                            <option value="{{ $department->id }}">{{ $department->name }}</option>
                                        @endforeach
                                    </select>
                                </div>
                                <div class="form-group">
                                    <label for="course_title">Course Title</label>
                                    <input type="text" name="course_title" value="{{ old('course_title') }}" placeholder="Course Title" id="course_title" class="form-control" required>
                                </div>
                                <div class="form-group">
                                    <label for="download_url">Upload Course Outline</label>
                                    <input type="file" name="download_url"  id="download_url" class="form-control">
                                </div>
                                <div class="form-group">
                                    <label for="about_course">About Course</label>
                                    <textarea name="about_course" id="about_course" rows="5" class="editor form-control" >{{ old('about_course') }}</textarea>
                                </div>
                            </div>
                            <!-- /.box-body -->
                            @can('add_course_outline')
                                <div class="box-footer">
                                    <button class="btn btn-primary" type="submit">Submit</button>
                                </div>
                            @endcan
                        </div>
                    </div>
                {!! Form::close() !!}
            </div>
            <!-- ./row -->
        </section>
        <!-- /.content -->
    </div>
@endsection

@section('js')
    <script src="https://cdn.ckeditor.com/4.7.3/standard/ckeditor.js"></script>
    <script>
        CKEDITOR.replaceClass='editor';
    </script>
@endsection