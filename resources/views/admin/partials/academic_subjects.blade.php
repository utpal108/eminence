<table class="table table-bordered table-condesed">
    <thead>
    <tr>
        <th class="text-center">SL</th>
        <th class="text-center">Subject</th>
        <th class="text-center">Department</th>
        <th class="text-center">Group</th>
        <th class="text-center">Section</th>
        <th class="text-center">Action</th>
    </tr>
    </thead>
    <tbody id="sortable">
    @php($sl=1)
    @foreach($all_subjects as $subject)
        <tr class="text-center ic-single-item ui-state-default" id="{{ $subject->id }}" >
            <td>{{ $sl }}</td>
            <td>{{ $subject->subject_name }}</td>
            <td>{{ $subject->department['name'] }}</td>
            <td>{{ $subject->group['group_name'] }}</td>
            <td>{{ $subject->section['section_name'] }}</td>
            <td>
                @can('edit_subject')
                    <a title="Edit" class="btn btn-xs btn-success edit-row" href="{{ action('SubjectController@edit',['id'=>$subject->id]) }}">
                        Edit
                    </a>&nbsp;
                @endcan
                @can('delete_subject')
                    {!! Form::open(['action'=>['SubjectController@destroy',$subject->id],'method'=>'delete', 'style'=>'display:inline']) !!}
                    <button title="Delete" class="btn btn-xs btn-danger delete-row" onclick="return confirm('Are You Sure ?')">
                        Delete
                    </button>
                    {!! Form::close() !!}
                @endcan
            </td>
        </tr>
        @php($sl++)
    @endforeach
    </tbody>
</table>