@extends('admin.partials.main')
@section('content')
    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <h1>
                Messages
                <small>All Messages</small>
            </h1>
            <ol class="breadcrumb">
                <li><i class="fa fa-dashboard"></i> <a href="#">Dashboard</a></li>
                <li class="active">Messages</li>
            </ol>
        </section>

        <!-- Main content -->
        <section class="content">
            <div class="row">
                <div class="col-xs-12">
                    <div class="box">
                        @can('add_message')
                            <div class="box-header">
                                <div class="pull-left">
                                    <a id="add-button" title="Add New Message" class="btn btn-success" href="{{ action('MessageForUniversityController@create') }}"><i class="fa fa-plus-circle"></i> Add New Message</a>
                                </div>
                            </div>
                        @endcan
                        <!-- /.box-header -->
                        <div class="box-body table-responsive">
                            <table class="table table-bordered table-condesed">
                                <thead>
                                <tr>
                                    <th class="text-center">SL</th>
                                    <th class="text-center">Message Title</th>
                                    <th class="text-center">Provide By</th>
                                    <th class="text-center">Action</th>
                                </tr>
                                </thead>
                                <tbody>
                                @php($sl=(($messages->currentPage()-1)*$messages->perPage())+1)
                                @foreach($messages as $message)
                                    <tr class="text-center">
                                        <td>{{ $sl }}</td>
                                        <td>{{ $message->message_title }}</td>
                                        <td>{{ $message->provide_by }}</td>
                                        <td>
                                            @can('edit_message')
                                                <a title="Edit" class="btn btn-xs btn-default edit-row" href="{{ action('MessageForUniversityController@edit',['id'=>$message->id]) }}">
                                                    <i class="fa fa-edit"></i>
                                                </a>
                                            @endcan
                                            @can('delete_message')
                                                {!! Form::open(['action'=>['MessageForUniversityController@destroy',$message->id],'method'=>'delete','style'=>'display:inline']) !!}
                                                    <button title="Delete" class="btn btn-xs btn-danger delete-row" type="submit" onclick="return confirm('Are You Sure ?')">
                                                        <i class="fa fa-times"></i>
                                                    </button>
                                                {!! Form::close() !!}
                                            @endcan
                                        </td>
                                    </tr>
                                    @php($sl++)
                                @endforeach
                                </tbody>
                            </table>
                        </div>
                        <!-- /.box-body -->
                        <div class="box-footer clearfix">
                            {{ $messages->links() }}
                        </div>
                    </div>
                    <!-- /.box -->
                </div>
            </div>
            <!-- ./row -->
        </section>
        <!-- /.content -->
    </div>
    <!-- /.content-wrapper -->
@endsection