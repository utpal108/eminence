@extends('admin.partials.main')
@section('content')
    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <h1>
                Sliders
                <small>All Sliders</small>
            </h1>
            <ol class="breadcrumb">
                <li><i class="fa fa-dashboard"></i> <a href="#">Dashboard</a></li>
                <li class="active">Sliders</li>
            </ol>
        </section>

        <!-- Main content -->
        <section class="content">
            <div class="row">
                <div class="col-xs-12">
                    <div class="box">
                        @can('add_slider')
                            <div class="box-header">
                                <div class="pull-left">
                                    <a id="add-button" title="Add New Slider" class="btn btn-success" href="{{ action('AdminSliderController@create') }}"><i class="fa fa-plus-circle"></i> Add New Slider</a>
                                </div>
                            </div>
                        @endcan
                        <!-- /.box-header -->
                        <div class="box-body table-responsive">
                            <table class="table table-bordered table-condesed">
                                <thead>
                                <tr>
                                    <th class="text-center">SL</th>
                                    <th class="text-center">Title</th>
                                    <th class="text-center">Subtitle</th>
                                    <th class="text-center">Image</th>
                                    <th class="text-center">Action</th>
                                </tr>
                                </thead>
                                <tbody>
                                @php($sl=(($sliders->currentPage()-1)*$sliders->perPage())+1)
                                @foreach($sliders as $slider)
                                    <tr class="text-center">
                                        <td>{{ $sl }}</td>
                                        <td>{{ $slider->title }}</td>
                                        <td>{!!  str_limit($slider->subtitle,100,'(...)') !!}</td>
                                        <td><img src="{{ Storage::url($slider->bg_image) }}" class="img-responsive img-thumbnail" style="max-height: 100px; max-width: 100px"></td>
                                        <td>
                                            @can('edit_slider')
                                                <a title="Edit" class="btn btn-xs btn-default edit-row" href="{{ action('AdminSliderController@edit',['id'=>$slider->id]) }}">
                                                    <i class="fa fa-edit"></i>
                                                </a>
                                            @endcan
                                            @can('delete_slider')
                                                {!! Form::open(['action'=>['AdminSliderController@destroy',$slider->id],'method'=>'Delete','style'=>'display:inline']) !!}
                                                    <button type="submit" onclick="return confirm('Are You Sure ?')" class="btn btn-xs btn-danger delete-row"><i class="fa fa-times"></i></button>
                                                {!! Form::close() !!}
                                            @endcan
                                        </td>
                                    </tr>
                                    @php($sl++)
                                @endforeach
                                </tbody>
                            </table>
                        </div>
                        <!-- /.box-body -->
                        <div class="box-footer clearfix">
                            {{ $sliders->links() }}
                        </div>
                    </div>
                    <!-- /.box -->
                </div>
            </div>
            <!-- ./row -->
        </section>
        <!-- /.content -->
    </div>
    <!-- /.content-wrapper -->
@endsection