@extends('admin.partials.main')

@section('content')
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <h1>
                Add New Payment
            </h1>
            <ol class="breadcrumb">
                <li><a href="#"><i class="fa fa-dashboard"></i> Dashboard</a></li>
                <li><a href="#">Payment</a></li>
                <li class="active">Add New Payment</li>
            </ol>
        </section>

        <!-- Main content -->
        <section class="content">
            <div class="row">
                {!! Form::open(['action'=>'PaymentStructureController@store']) !!}
                <div class="col-xs-9">
                    <div class="box">
                        <!-- form start -->
                        <div class="box-body">
                            @if (session('error'))
                                <div class="alert alert-danger">
                                    {{ session('error') }}
                                </div>
                            @endif
                            <div class="form-group">
                                <label for="department_id">Department Name</label>
                                <select name="department_id" id="department_id" class="form-control select2 select2-hidden-accessible" tabindex="-1" aria-hidden="true">
                                    <option value="">-- Select Department --</option>
                                    @foreach($all_departments as $department)
                                        <option value="{{ $department->id }}">{{ $department->name }}</option>
                                    @endforeach
                                </select>
                            </div>
                            <div class="form-group">
                                <label for="group_id">Group Name</label>
                                <select name="group_id" id="group_id" class="form-control select2 select2-hidden-accessible" tabindex="-1" aria-hidden="true"></select>
                            </div>
                            <div class="form-group">
                                <label for="section_id">Semester Name</label>
                                <select name="section_id" id="section_id" class="form-control select2 select2-hidden-accessible" tabindex="-1" aria-hidden="true"></select>
                            </div>
                            <div class="form-group">
                                <label for="session_id">Session Name</label>
                                <select name="session_id" id="session_id" class="form-control select2 select2-hidden-accessible" tabindex="-1" aria-hidden="true">
                                    <option value="">-- Select Session --</option>
                                    @foreach($all_sessions as $session)
                                        <option value="{{ $session->id }}">{{ $session->session_name }}</option>
                                    @endforeach
                                </select>
                            </div>
                            <div class="form-group">
                                <label for="payment_type">Payment Type</label>
                                <select name="payment_type" id="payment_type" class="form-control" tabindex="-1" aria-hidden="true">
                                    <option value="">-- Select Payment Type --</option>
                                    <option value="tution_fee">Tution Fee</option>
                                    <option value="others">Others</option>
                                </select>
                            </div>
                            <div class="form-group">
                                <label for="payment_amount">Amount</label>
                                <input type="text" name="amount" placeholder="Payment Amount" id="payment_amount" class="form-control" required>
                            </div>
                        </div>
                        <!-- /.box-body -->
                        @can('add_payment_structure')
                            <div class="box-footer">
                                <button class="btn btn-primary" type="submit">Submit</button>
                            </div>
                        @endcan
                    </div>
                </div>
                {!! Form::close() !!}
            </div>
            <!-- ./row -->
        </section>
        <!-- /.content -->
    </div>
@endsection

@section('js')
    <script src="https://cdn.ckeditor.com/4.7.3/standard/ckeditor.js"></script>
    <script>
        CKEDITOR.replaceClass='editor';
    </script>
    <script>
        $(document).ready(function () {
            $('#department_id').change(function (e) {
                $('#section_id').html('');
                var department=$('#department_id').val();
                $.ajax({
                    type:'GET',
                    url:'/ajax_call/department_wise_data',
                    data:{
                        'department_id':department
                    },
                    success:function (response) {
                        var res=JSON.parse(response);
                        $('#group_id').html(res.groups);
                    }
                })
            });
            $('#group_id').change(function (e) {
                var department=$('#department_id').val();
                var group_id=$('#group_id').val();

                $.ajax({
                    type:'GET',
                    url:'/admin/group_wise_data',
                    data:{
                        department_id:department,
                        group_id:group_id,
                    },
                    success:function (response) {
                        var res=JSON.parse(response);
                        $('#section_id').html(res.sections);
                    }
                })
            })
        })
    </script>
@endsection