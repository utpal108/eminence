@extends('admin.partials.main')
@section('content')
    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <h1>
                Payments
                <small>All Payment Structure</small>
            </h1>
            <ol class="breadcrumb">
                <li>
                    <i class="fa fa-dashboard"></i> <a href="#">Dashboard</a>
                </li>
                <li class="active">All Payment Structure</li>
            </ol>
        </section>

        <!-- Main content -->
        <section class="content">
            <div class="row">
                <div class="col-xs-12">
                    <div class="box">
                        @can('add_payment_structure')
                            <div class="box-header">
                                <div class="pull-left">
                                    <a id="add-button" title="Add New Department" class="btn btn-success" href="{{ action('PaymentStructureController@create') }}"><i class="fa fa-plus-circle"></i> New Payment</a>
                                </div>
                            </div>
                        @endcan
                        <!-- /.box-header -->
                        <div class="box-body table-responsive">
                            <table class="table table-bordered table-condesed">
                                <thead>
                                <tr>
                                    <th class="text-center">SL</th>
                                    <th class="text-center">Department</th>
                                    <th class="text-center">Group</th>
                                    <th class="text-center">Semester</th>
                                    <th class="text-center">Session</th>
                                    <th class="text-center">amount</th>
                                    <th class="text-center">Action</th>
                                </tr>
                                </thead>
                                <tbody>
                                @php($sl=(($all_payments->currentPage()-1)*$all_payments->perPage())+1)
                                @foreach($all_payments as $payment)
                                    <tr class="text-center">
                                        <td>{{ $sl }}</td>
                                        <td>{{ $payment->department['name'] }}</td>
                                        <td>{{ $payment->group['group_name'] }}</td>
                                        <td>{{ $payment->section['section_name'] }}</td>
                                        <td>{{ $payment->session['session_name'] }}</td>
                                        <td>{{ $payment->amount }}</td>
                                        <td>
                                            @can('edit_payment_structure')
                                                <a title="Edit" class="btn btn-xs btn-success edit-row" href="{{ action('PaymentStructureController@edit',['id'=>$payment->id]) }}">
                                                    Edit
                                                </a>&nbsp;&nbsp;
                                            @endcan
                                            @can('delete_payment_structure')
                                                {!! Form::open(['action'=>['PaymentStructureController@destroy',$payment->id],'method'=>'delete', 'style'=>'display:inline']) !!}
                                                    <button title="Delete" class="btn btn-xs btn-danger delete-row" onclick="return confirm('Are You Sure ?')">
                                                        Delete
                                                    </button>
                                                {!! Form::close() !!}
                                            @endcan
                                        </td>
                                    </tr>
                                    @php($sl++)
                                @endforeach
                                </tbody>
                            </table>
                        </div>
                        <!-- /.box-body -->
                        <div class="box-footer clearfix">
                            {{ $all_payments->links() }}
                        </div>
                    </div>
                    <!-- /.box -->
                </div>
            </div>
            <!-- ./row -->
        </section>
        <!-- /.content -->
    </div>
    <!-- /.content-wrapper -->
@endsection