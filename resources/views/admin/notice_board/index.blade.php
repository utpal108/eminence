@extends('admin.partials.main')
@section('content')
    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <h1>
                Notice Board
                <small>All Notice</small>
            </h1>
            <ol class="breadcrumb">
                <li><i class="fa fa-dashboard"></i> <a href="#">Dashboard</a></li>
                <li class="active">All Notice</li>
            </ol>
        </section>

        <!-- Main content -->
        <section class="content">
            <div class="row">
                <div class="col-xs-12">
                    <div class="box">
                        @can('add_notice_board')
                            <div class="box-header">
                                <div class="pull-left ic_margin_left15">
                                    <a id="add-button" title="Add New Section" class="btn btn-success" href="{{ action('NoticeBoardController@create') }}"><i class="fa fa-plus-circle"></i> Add Notice</a>
                                </div>
                            </div>
                        @endcan
                    <!-- /.box-header -->
                        <div class="box-body table-responsive">
                            <section class="ic-admin-menu">
                                <div class="col-xs-12">
                                    <div id="sortable">
                                        @foreach($all_notice as $notice)
                                            <div id="{{ $notice->id }}" class="ic-single-menu ui-state-default">
                                                <div class="ic-left">
                                                    <i class="fa fa-bars" aria-hidden="true"></i>&nbsp;&nbsp;&nbsp;<strong>{{ $notice->title }}</strong>
                                                </div>
                                                <div class="ic-right ic-btn">
                                                    @can('edit_notice_board')
                                                        <a title="Edit" class="edit btn btn-xs btn-default edit-row" href="{{ action('NoticeBoardController@edit',['id'=>$notice->id]) }}">
                                                            <i class="fa fa-edit"></i> Edit
                                                        </a>&nbsp;
                                                    @endcan
                                                    @can('delete_notice_board')
                                                        {!! Form::open(['action'=>['NoticeBoardController@destroy',$notice->id],'method'=>'Delete','style'=>'display:inline']) !!}
                                                            <button type="submit" onclick="return confirm('Are You Sure ?')" class="delete btn btn-xs btn-danger delete-row"><i class="fa fa-times"></i> Delete</button>
                                                        {!! Form::close() !!}
                                                    @endcan
                                                </div>
                                            </div>
                                        @endforeach
                                    </div>
                                </div>
                            </section>
                        </div>
                        <!-- /.box-body -->
                        {{--<div class="box-footer clearfix">--}}
                            {{--{{ $all_notice->links() }}--}}
                        {{--</div>--}}
                    </div>
                    <!-- /.box -->
                </div>
            </div>
            <!-- ./row -->
        </section>
        <!-- /.content -->
    </div>
    <!-- /.content-wrapper -->
@endsection

@section('js')
    <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
    <script src="{{ asset('ic_admin/js/jquery.toaster.js') }}"></script>
    <script>
        $( function() {
            $( "#sortable" ).sortable({
                stop: function(){
                    $.map($(this).find('.ic-single-menu'), function(el) {
                        var itemID = el.id;
                        var itemIndex = $(el).index();
                        $.ajax({
                            url:'{{URL::to("admin/notice_board/order")}}',
                            type:'GET',
                            dataType:'json',
                            data: {itemID:itemID, itemIndex: itemIndex},
                        })
                    });
                    $.toaster('Order Changed Successfully!', 'Status');
                }
            });
            $( "#sortable" ).disableSelection();
        } );
    </script>
@endsection