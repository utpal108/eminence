@extends('admin.partials.main')

@section('content')
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <h1>
                Update Exam Category
            </h1>
            @if(session()->has('error'))
                <br>
                <span class="text-danger">{{ session('error') }}</span>
            @endif
            <ol class="breadcrumb">
                <li><a href="#"><i class="fa fa-dashboard"></i> Dashboard</a></li>
                <li><a href="#">Exam Category</a></li>
                <li class="active">Update Exam Category</li>
            </ol>
        </section>

        <!-- Main content -->
        <section class="content">
            <div class="row">
                {!! Form::open(['action'=>['ExamCategoryController@update',$exam_category->id],'method'=>'put']) !!}
                <div class="col-xs-9">
                    @if($errors->any())
                        <div class="alert alert-danger">
                            <ul>
                                @foreach ($errors->all() as $error)
                                    <li>{{ $error }}</li>
                                @endforeach
                            </ul>
                        </div>
                    @elseif(session()->has('success'))
                        <div class="alert alert-success">
                            {{ session('success') }}
                        </div>
                    @endif
                    <div class="box">
                        <!-- form start -->
                        <div class="box-body">
                            <div class="form-group">
                                <label for="exam_name">Exam Category</label>
                                <input type="text" name="exam_name" value="{{ $exam_category->exam_name }}" placeholder="Enter Exam Name" id="exam_name" class="form-control" required>
                            </div>
                            <div class="form-group">
                                <label for="exam_type">Exam Type</label>
                                <select name="exam_type" id="exam_type" class="form-control">
                                    <option value="general" @if($exam_category->exam_type == 'general'){{ 'selected' }} @endif >General</option>
                                    <option value="tutorial" @if($exam_category->exam_type == 'tutorial'){{ 'selected' }} @endif >Tutorial</option>
                                </select>
                            </div>
                            <div class="form-group">
                                <label for="about">About Exam Type</label>
                                <textarea name="details" id="about" class="form-control editor">{{ $exam_category->details }}</textarea>
                            </div>

                        </div>
                        <!-- /.box-body -->
                        @can('edit_exam_category')
                            <div class="box-footer">
                                <button class="btn btn-primary" type="submit">Update</button>
                            </div>
                        @endcan
                    </div>
                </div>
                {!! Form::close() !!}
            </div>
            <!-- ./row -->
        </section>
        <!-- /.content -->
    </div>
@endsection

@section('js')
    <script src="https://cdn.ckeditor.com/4.7.3/standard/ckeditor.js"></script>
    <script>
        CKEDITOR.replaceClass='editor';
    </script>
@endsection