@extends('admin.partials.main')
@section('content')
    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <h1>
                Photo Category
                <small>All Category</small>
            </h1>
            <ol class="breadcrumb">
                <li><i class="fa fa-dashboard"></i> <a href="#">Dashboard</a></li>
                <li class="active">All Category</li>
            </ol>
        </section>

        <!-- Main content -->
        <section class="content">
            <div class="row">
                <div class="col-xs-12">
                    <div class="box">
                        @can('add_photo_category')
                            <div class="box-header">
                                <div class="pull-left">
                                    <a id="add-button" title="Add New Category" class="btn btn-success" href="{{ action('PhotoCategoryController@create') }}"><i class="fa fa-plus-circle"></i> Add New Category</a>
                                </div>
                            </div>
                        @endcan
                        <!-- /.box-header -->
                        <div class="box-body table-responsive">
                            <table class="table table-bordered table-condesed">
                                <thead>
                                <tr>
                                    <th class="text-center">SL</th>
                                    <th class="text-center">Category Name</th>
                                    <th class="text-center">Action</th>
                                </tr>
                                </thead>
                                <tbody>
                                @php($sl=(($all_categories->currentPage()-1)*$all_categories->perPage())+1)
                                @foreach($all_categories as $category)
                                    <tr class="text-center">
                                        <td>{{ $sl }}</td>
                                        <td>{{ $category->name }}</td>
                                        <td>
                                            @can('edit_photo_category')
                                                <a title="Edit" class="btn btn-xs btn-success edit-row" href="{{ action('PhotoCategoryController@edit',['id'=>$category->id]) }}">
                                                    Edit
                                                </a>&nbsp;
                                            @endcan
                                            @can('delete_photo_category')
                                                {!! Form::open(['action'=>['PhotoCategoryController@destroy',$category->id],'method'=>'delete', 'style'=>'display:inline']) !!}
                                                    <button class="btn btn-xs btn-danger delete-row" type="submit" onclick="return confirm('Are You Sure ?')">
                                                        Delete
                                                    </button>
                                                {!! Form::close() !!}
                                            @endcan
                                        </td>
                                    </tr>
                                    @php($sl++)
                                @endforeach
                                </tbody>
                            </table>
                        </div>
                        <!-- /.box-body -->
                        <div class="box-footer clearfix">
                            {{ $all_categories->links() }}
                        </div>
                    </div>
                    <!-- /.box -->
                </div>
            </div>
            <!-- ./row -->
        </section>
        <!-- /.content -->
    </div>
    <!-- /.content-wrapper -->
@endsection