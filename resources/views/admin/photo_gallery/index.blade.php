@extends('admin.partials.main')
@section('content')
    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <h1>
                Photo Gallery
                <small>All Photos</small>
            </h1>
            <ol class="breadcrumb">
                <li><i class="fa fa-dashboard"></i> <a href="#">Dashboard</a></li>
                <li class="active">All Photos</li>
            </ol>
        </section>

        <!-- Main content -->
        <section class="content">
            <div class="row">
                <div class="col-xs-12">
                    <div class="box">
                        @can('add_photo')
                            <div class="box-header">
                                <div class="pull-left">
                                    <a id="add-button" title="Add New Photo" class="btn btn-success" href="{{ action('PhotoGalleryController@create') }}"><i class="fa fa-plus-circle"></i> Add New Photo</a>
                                </div>
                            </div>
                        @endcan
                        <!-- /.box-header -->
                        <div class="box-body table-responsive">
                            <table class="table table-bordered table-condesed">
                                <thead>
                                <tr>
                                    <th class="text-center">SL</th>
                                    <th class="text-center">Name</th>
                                    <th class="text-center">Photo</th>
                                    <th class="text-center">Action</th>
                                </tr>
                                </thead>
                                <tbody>
                                @php($sl=(($all_photos->currentPage()-1)*$all_photos->perPage())+1)
                                @foreach($all_photos as $photo)
                                    <tr class="text-center">
                                        <td>{{ $sl }}</td>
                                        <td>{{ $photo->name }}</td>
                                        <td class="text-center"><img src="{{ Storage::url($photo->photo) }}" class="img-responsive img-thumbnail" style="max-height: 60px"></td>
                                        <td>
                                            @can('edit_photo')
                                                <a title="Edit" class="btn btn-xs btn-success edit-row" href="{{ action('PhotoGalleryController@edit',['id'=>$photo->id]) }}">
                                                    Edit
                                                </a>&nbsp;
                                            @endcan
                                            @can('delete_photo')
                                                {!! Form::open(['action'=>['PhotoGalleryController@destroy',$photo->id],'method'=>'delete', 'style'=>'display:inline']) !!}
                                                    <button class="btn btn-xs btn-danger delete-row" type="submit" onclick="return confirm('Are You Sure ?')">
                                                        Delete
                                                    </button>
                                                {!! Form::close() !!}
                                            @endcan
                                        </td>
                                    </tr>
                                    @php($sl++)
                                @endforeach
                                </tbody>
                            </table>
                        </div>
                        <!-- /.box-body -->
                        <div class="box-footer clearfix">
                            {{ $all_photos->links() }}
                        </div>
                    </div>
                    <!-- /.box -->
                </div>
            </div>
            <!-- ./row -->
        </section>
        <!-- /.content -->
    </div>
    <!-- /.content-wrapper -->
@endsection