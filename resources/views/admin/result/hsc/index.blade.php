@extends('admin.partials.main')
@section('content')
    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <h1>
                HSC Result
                <small>All HSC Result</small>
            </h1>
            <ol class="breadcrumb">
                <li>
                    <i class="fa fa-dashboard"></i> <a href="#">Dashboard</a>
                </li>
                <li class="active">Add HSC Result</li>
            </ol>
        </section>

        <!-- Main content -->
        <section class="content">
            <div class="row">
                <div class="col-xs-12">
                    <div class="box">
                        <div class="box-header">
                            <div class="pull-left">
                                <a id="add-button" title="Add New HSC Result" class="btn btn-success" href="{{ action('HscResultController@create') }}"><i class="fa fa-plus-circle"></i> Add HSC Result</a>
                            </div>
                        </div>
                        <!-- /.box-header -->
                        <div class="box-body table-responsive">
                            <table class="table table-bordered table-condesed">
                                <thead>
                                <tr>
                                    <th class="text-center">SL</th>
                                    <th class="text-center">Year</th>
                                    <th class="text-center">About Result</th>
                                    <th class="text-center">Action</th>
                                </tr>
                                </thead>
                                <tbody>
                                @php($sl=(($all_hsc_results->currentPage()-1)*$all_hsc_results->perPage())+1)
                                @foreach($all_hsc_results as $hsc_result)
                                    <tr class="text-center">
                                        <td>{{ $sl }}</td>
                                        <td>{{ $hsc_result->year }}</td>
                                        <td>{!! str_limit($hsc_result->about,150,'(...)') !!}</td>
                                        <td>
                                            <a title="Edit" class="btn btn-xs btn-success edit-row" href="{{ action('HscResultController@edit',['id'=>$hsc_result->id]) }}">
                                                Edit
                                            </a>&nbsp;
                                            {!! Form::open(['action'=>['HscResultController@destroy',$hsc_result->id],'method'=>'delete', 'style'=>'display:inline']) !!}
                                                <button title="Delete" class="btn btn-xs btn-danger delete-row" onclick="return confirm('Are You Sure')">
                                                    Delete
                                                </button>
                                            {!! Form::close() !!}
                                        </td>
                                    </tr>
                                    @php($sl++)
                                @endforeach
                                </tbody>
                            </table>
                        </div>
                        <!-- /.box-body -->
                        <div class="box-footer clearfix">
                            {{ $all_hsc_results->links() }}
                        </div>
                    </div>
                    <!-- /.box -->
                </div>
            </div>
            <!-- ./row -->
        </section>
        <!-- /.content -->
    </div>
    <!-- /.content-wrapper -->
@endsection