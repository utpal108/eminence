@extends('admin.partials.main')
@section('content')
    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <h1>
                Result Grade
                <small>All Result Grade</small>
            </h1>
            <ol class="breadcrumb">
                <li>
                    <i class="fa fa-dashboard"></i> <a href="#">Dashboard</a>
                </li>
                <li class="active">All Result Grade</li>
            </ol>
        </section>

        <!-- Main content -->
        <section class="content">
            <div class="row">
                <div class="col-xs-12">
                    <div class="box">
                        @can('add_result_grade')
                            <div class="box-header">
                                <div class="pull-left">
                                    <a id="add-button" title="Add Result Grade" class="btn btn-success" href="{{ action('ResultGradeController@create') }}"><i class="fa fa-plus-circle"></i> Add New Grade</a>
                                </div>
                            </div>
                        @endcan
                        <!-- /.box-header -->
                        <div class="box-body table-responsive">
                            <table class="table table-bordered table-condesed">
                                <thead>
                                <tr>
                                    <th class="text-center">SL</th>
                                    <th class="text-center">Mark Range From</th>
                                    <th class="text-center">Mark Range To</th>
                                    <th class="text-center">GPA</th>
                                    <th class="text-center">Grade</th>
                                    <th class="text-center">Action</th>
                                </tr>
                                </thead>
                                <tbody>
                                @php($sl=(($result_grades->currentPage()-1)*$result_grades->perPage())+1)
                                @foreach($result_grades as $result_grade)
                                    <tr class="text-center">
                                        <td>{{ $sl }}</td>
                                        <td>{{ $result_grade->mark_range_from }}</td>
                                        <td>{!! $result_grade->mark_range_to !!} </td>
                                        <td>{!! $result_grade->gpa !!} </td>
                                        <td>{!! $result_grade->grade !!} </td>
                                        <td>
                                            @can('edit_result_grade')
                                                <a title="Edit" class="btn btn-xs btn-success edit-row" href="{{ action('ResultGradeController@edit',['id'=>$result_grade->id]) }}">
                                                    Edit
                                                </a>&nbsp;
                                            @endcan
                                            @can('delete_result_grade')
                                                {!! Form::open(['action'=>['ResultGradeController@destroy',$result_grade->id],'method'=>'delete', 'style'=>'display:inline']) !!}
                                                    <button title="Delete" class="btn btn-xs btn-danger delete-row" onclick="return confirm('Are You Sure ?')">
                                                        Delete
                                                    </button>
                                                {!! Form::close() !!}
                                            @endcan
                                        </td>
                                    </tr>
                                    @php($sl++)
                                @endforeach
                                </tbody>
                            </table>
                        </div>
                        <!-- /.box-body -->
                        <div class="box-footer clearfix">
                            {{ $result_grades->links() }}
                        </div>
                    </div>
                    <!-- /.box -->
                </div>
            </div>
            <!-- ./row -->
        </section>
        <!-- /.content -->
    </div>
    <!-- /.content-wrapper -->
@endsection