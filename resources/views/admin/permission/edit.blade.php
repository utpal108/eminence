@extends('admin.partials.main')

@section('content')
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <h1>
                Edit Permission
            </h1>
            <ol class="breadcrumb">
                <li><a href="#"><i class="fa fa-dashboard"></i> Dashboard</a></li>
                <li><a href="#">Permission</a></li>
                <li class="active">Edit Permission</li>
            </ol>
        </section>

        <!-- Main content -->
        <section class="content">
            <div class="row">
                {!! Form::open(['action'=>['PermissionController@update',$permission->id], 'method'=>'PUT']) !!}
                <div class="col-xs-9">
                    @if ($errors->any())
                        <div class="alert alert-danger">
                            <ul>
                                @foreach ($errors->all() as $error)
                                    <li>{{ $error }}</li>
                                @endforeach
                            </ul>
                        </div>
                    @endif
                    <div class="box">
                        <!-- form start -->
                        <div class="box-body">
                            <div class="form-group">
                                <label for="name">Permission</label>
                                <input type="text" name="name" value="{{ $permission->name }}" class="form-control" placeholder="Permission Name" id="name">
                            </div>
                            <div class="form-group">
                                <label for="module_name">Module Name</label>
                                <input type="text" name="module_name" value="{{ $permission->module_name }}" class="form-control" placeholder="Module Name" id="module_name">
                            </div>
                        </div>
                        <!-- /.box-body -->
                        <div class="box-footer">
                            <button class="btn btn-primary" type="submit">Submit</button>
                        </div>
                    </div>
                </div>
                {!! Form::close() !!}
            </div>
            <!-- ./row -->
        </section>
        <!-- /.content -->
    </div>
@endsection
