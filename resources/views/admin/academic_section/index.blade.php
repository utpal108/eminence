@extends('admin.partials.main')
@section('content')
    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <h1>
                Academic Sections
                <small>All Sections</small>
            </h1>
            <ol class="breadcrumb">
                <li>
                    <i class="fa fa-dashboard"></i> <a href="#">Dashboard</a>
                </li>
                <li class="active">All Sections</li>
            </ol>
        </section>

        <!-- Main content -->
        <section class="content">
            <div class="row">
                <div class="col-xs-12">
                    <div class="box">
                        @can('add_section')
                            <div class="box-header">
                                <div class="pull-left">
                                    <a id="add-button" title="Add New Section" class="btn btn-success" href="{{ action('AcademicSectionController@create') }}"><i class="fa fa-plus-circle"></i> Add New Section</a>
                                </div>
                            </div>
                        @endcan
                        <!-- /.box-header -->
                        <div class="box-body table-responsive">
                            <table class="table table-bordered table-condesed">
                                <thead>
                                <tr>
                                    <th class="text-center">SL</th>
                                    <th class="text-center">Section Name</th>
                                    <th class="text-center">About Section</th>
                                    <th class="text-center">Action</th>
                                </tr>
                                </thead>
                                <tbody>
                                @php($sl=(($all_sections->currentPage()-1)*$all_sections->perPage())+1)
                                @foreach($all_sections as $section)
                                    <tr class="text-center">
                                        <td>{{ $sl }}</td>
                                        <td>{{ $section->section_name }}</td>
                                        <td>{!! str_limit($section->about_section,150,'(...)') !!}</td>
                                        <td>
                                            @can('edit_section')
                                                <a title="Edit" class="btn btn-xs btn-success edit-row" href="{{ action('AcademicSectionController@edit',['id'=>$section->id]) }}">
                                                    Edit
                                                </a>&nbsp;
                                            @endcan
                                            @can('delete_section')
                                                {!! Form::open(['action'=>['AcademicSectionController@destroy',$section->id],'method'=>'delete', 'style'=>'display:inline']) !!}
                                                    <button title="Delete" class="btn btn-xs btn-danger delete-row" onclick="return confirm('Are You Sure ?')">
                                                        Delete
                                                    </button>
                                                {!! Form::close() !!}
                                            @endcan
                                        </td>
                                    </tr>
                                    @php($sl++)
                                @endforeach
                                </tbody>
                            </table>
                        </div>
                        <!-- /.box-body -->
                        <div class="box-footer clearfix">
                            {{ $all_sections->links() }}
                        </div>
                    </div>
                    <!-- /.box -->
                </div>
            </div>
            <!-- ./row -->
        </section>
        <!-- /.content -->
    </div>
    <!-- /.content-wrapper -->
@endsection