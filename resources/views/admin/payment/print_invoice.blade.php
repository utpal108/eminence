<!DOCTYPE html>
<html>
<head>
    <style>
        table {
            font-family: arial, sans-serif;
            border-collapse: collapse;
            width: 100%;
        }

        td, th {
            border: 1px solid #dddddd;
            text-align: left;
            padding: 8px;
        }

        th {
            background-color: #dddddd;
        }
    </style>
</head>
<body>

<h2>Invoice</h2>
<h3>No: {{ $invoice->id }}</h3>

<table>
    <tr>
        <th>SL</th>
        <th>Payment Type</th>
        <th>Amount</th>
    </tr>

    @php($sl=1)
    @php($total_amount=0)
    @foreach($invoice->payments as $payment)
        <tr>
            <td>{{ $sl }}</td>
            <td>{{ $payment['payment_type'] }}</td>
            <td>{{ $payment['payment_amount'] }}</td>
        </tr>
        @php($total_amount=$total_amount+$payment['payment_amount'])
        @php($sl++)
    @endforeach
    <tr>
        <td colspan="2">Total</td>
        <td>{{ $total_amount }}</td>
    </tr>
</table>

</body>
</html>
