@extends('admin.partials.main')
@section('content')
    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <h1>
                Payments
                <small>All Payments</small>
            </h1>
            <ol class="breadcrumb">
                <li>
                    <i class="fa fa-dashboard"></i> <a href="#">Dashboard</a>
                </li>
                <li class="active">All Payment</li>
            </ol>
        </section>

        <!-- Main content -->
        <section class="content">
            <div class="row">
                <div class="col-xs-12">
                    <div class="box">
                        @can('add_payment')
                            <div class="box-header">
                                <div class="pull-left">
                                    <a id="add-button" title="Add New Department" class="btn btn-success" href="{{ action('StudentPaymentController@create') }}"><i class="fa fa-plus-circle"></i> New Payment</a>
                                </div>
                            </div>
                        @endcan
                        <!-- /.box-header -->
                        <div class="box-body table-responsive">
                            <table class="table table-bordered table-condesed">
                                <thead>
                                <tr>
                                    <th class="text-center">SL</th>
                                    <th class="text-center">Student ID</th>
                                    <th class="text-center">Payment Type</th>
                                    <th class="text-center">Payment Amount</th>
                                    <th class="text-center">Date</th>
                                    <th class="text-center">Action</th>
                                </tr>
                                </thead>
                                <tbody>
                                @php($sl=(($all_payments->currentPage()-1)*$all_payments->perPage())+1)
                                @foreach($all_payments as $payment)
                                    <tr class="text-center">
                                        <td>{{ $sl }}</td>
                                        <td>{{ $payment->student['student_id'] }}</td>
                                        <td>{{ $payment->payment_type }}</td>
                                        <td>{{ $payment->amount }}</td>
                                        <td>{{ $payment->created_at }}</td>
                                        <td>
                                            @can('edit_payment')
                                                <a title="Edit" class="btn btn-xs btn-success edit-row" href="{{ action('StudentPaymentController@edit',['id'=>$payment->id]) }}">
                                                    Edit
                                                </a>&nbsp;&nbsp;
                                            @endcan
                                            @can('delete_payment')
                                                {!! Form::open(['action'=>['StudentPaymentController@destroy',$payment->id],'method'=>'delete', 'style'=>'display:inline']) !!}
                                                    <button title="Delete" class="btn btn-xs btn-danger delete-row" onclick="return confirm('Are You Sure')">
                                                        Delete
                                                    </button>
                                                {!! Form::close() !!}
                                            @endcan
                                        </td>
                                    </tr>
                                    @php($sl++)
                                @endforeach
                                </tbody>
                            </table>
                        </div>
                        <!-- /.box-body -->
                        <div class="box-footer clearfix">
                            {{ $all_payments->links() }}
                        </div>
                    </div>
                    <!-- /.box -->
                </div>
            </div>
            <!-- ./row -->
        </section>
        <!-- /.content -->
    </div>
    <!-- /.content-wrapper -->
@endsection