@extends('admin.partials.main')

@section('content')
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <h1>
                Edit Student Payment
            </h1>
            <ol class="breadcrumb">
                <li><a href="#"><i class="fa fa-dashboard"></i> Dashboard</a></li>
                <li><a href="#">Payment</a></li>
                <li class="active">Edit Payment</li>
            </ol>
        </section>

        <!-- Main content -->
        <section class="content">
            <div class="row">
                <div class="col-xs-9">
                    <div class="box">
                        <div class="box-body">
                            @if (session('error'))
                                <div class="alert alert-danger">
                                    {{ session('error') }}
                                </div>
                            @endif
                            @if (session('success'))
                                <div class="alert alert-success">
                                    {{ session('success') }}
                                </div>
                            @endif
                            {!! Form::open(['action'=>'StudentPaymentController@edit_invoice','method'=>'GET']) !!}
                                <div class="form-group">
                                    <label for="invoice_no">Invoice No.</label>
                                    <div class="input-group">
                                        <input type="text" class="form-control" name="invoice_no" placeholder="Invoice No" value="{{ $previous_invoice }}">
                                        <span class="input-group-btn">
                                          <button type="submit" class="btn btn-info btn-flat">Find</button>
                                        </span>
                                    </div>
                                </div>
                            {!! Form::close() !!}
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                @if($invoice != null)
                    {!! Form::open(['action'=>['StudentPaymentController@update',$invoice->id],'method'=>'PUT']) !!}
                <div class="col-xs-9">
                    <div class="box">
                        <!-- form start -->
                        <div class="box-body input_form">
                            <div class="form-group">
                                <label for="student_id">Student ID</label>
                                <input type="text" name="student_id" placeholder="Student ID" id="student_id" class="form-control" value="{{ $invoice->student_id }}" required>
                            </div>

                                @foreach($invoice->payments as $payment)
                                    <div class="row input_payment" style="padding-top: 5px">
                                        <div class="col-xs-3">
                                            <select class="form-control payment_type" name="payment_type[]">
                                                <option value=""> -- Payment Type --</option>
                                                <option value="tuition" @if($payment['payment_type']=='tuition'){{ 'selected' }} @endif >Tuition Fee</option>
                                                <option value="due" @if($payment['payment_type']=='due'){{ 'selected' }} @endif>Due Payment</option>
                                                <option value="other" @if($payment['payment_type']=='other'){{ 'selected' }} @endif>Others</option>
                                            </select>
                                        </div>
                                        <div class="col-xs-4">
                                            <select class="form-control all_semester" name="section[]">
                                                @foreach($all_sections as $section)
                                                    <option value="{{ $section->id }}" @if($payment['section']==$section->id){{ 'selected' }} @endif>{{ $section->section_name }}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                        <div class="col-xs-4 input-group">
                                            <input type="text" class="form-control payment_amount" name="payment_amount[]" value="{{ $payment['payment_amount'] }}" placeholder="Payment Amount"> <span class="input-group-addon"><i class="fa fa-minus add_payment" onclick="$(this).parent().parent().parent().remove()"></i></span>
                                        </div>
                                    </div>
                                @endforeach
                        </div>
                        <!-- /.box-body -->
                        <div class="box-footer">
                            @can('print_invoice')
                                <button class="btn btn-primary" type="submit" name="invoice_submit" value="create_invoice">Print Invoice</button>
                            @endcan
                            <button type="submit" class="btn btn-success" name="invoice_submit" value="payment_paid">Payment Paid</button>
                            @can('add_payment')
                                <button type="button" class="btn btn-info pull-right add_payment">Add New Payment</button>&nbsp;&nbsp;
                            @endcan

                        </div>
                    </div>
                </div>
                    {!! Form::close() !!}
                @else
                    <div class="col-xs-9">
                        <h3>No Data Found</h3>
                    </div>
                @endif
            </div>
            <!-- ./row -->
        </section>
        <!-- /.content -->
    </div>
@endsection

@section('js')
    <script>
        $(document).ready(function () {

            function removeElement() {
                $(this).parent().parent().parent().remove();
            }

            var all_semester="<option value=''> -- Semester -- </option>"

            //    Get Student Sections
            $('#student_id').change(function () {
                var student_id=$('#student_id').val();
                $.ajax({
                    type:'GET',
                    url:'/ajax_call/get_student_sections',
                    data:{'student_id':student_id},
                    success:function (response) {
                        var res = JSON.parse(response);
                        $('.all_semester').html(res.sections);
                        all_semester=res.sections;
                    }
                })
            });


            // For Adding new Payment
            $('.add_payment').click(function (e) {
                $('.input_form').append('<div class="row input_payment" style="padding-top: 5px">\n' +
                    '                                    <div class="col-xs-3">\n' +
                    '                                        <select class="form-control payment_type" name="payment_type[]" >\n' +
                    '                                            <option value=""> -- Payment Type --</option>\n' +
                    '                                            <option value="tuition">Tuition Fee</option>\n' +
                    '                                            <option value="due">Due Payment</option>\n' +
                    '                                            <option value="other">Others</option>\n' +
                    '                                        </select>\n' +
                    '                                    </div>\n' +
                    '                                    <div class="col-xs-4">\n' +
                    '                                        <select class="form-control all_semester" name="section[]">\n' + all_semester +
                    '                                        </select>\n' +
                    '                                    </div>\n' +
                    '                                    <div class="col-xs-4 all_payment input-group">\n' +
                    '                                        <input type="text" class="form-control payment_amount" name="payment_amount[]" placeholder="Payment Amount"><span class="input-group-addon"><i class="fa fa-minus" onclick="$(this).parent().parent().parent().remove()"></i></span>\n' +
                    '                                    </div>\n' +
                    '                                </div>');
            });

            $('.input_form, .all_semester').on('change','select',function () {
                if ($(this).hasClass('payment_type')) {
                    var class_index=$('.payment_type').index(this);
                }
                else {
                    var class_index=$('.all_semester').index(this);
                }
                var student_id=$('#student_id').val();
                var section_id=$(".all_semester:eq("+class_index+")").val();
                var payment_type=$(".payment_type:eq("+class_index+")").val();

                if (student_id != '' && section_id != '' && payment_type=='tuition') {
                    $.ajax({
                        type:'GET',
                        url:'/ajax_call/get_tution_fee',
                        data:{'section_id':section_id, 'student_id':student_id},
                        success:function (response) {
                            var res = JSON.parse(response);
                            $(".payment_amount:eq("+class_index+")").val(res.tution_fee);
                        }
                    })
                }
                else {
                    $(".payment_amount:eq("+class_index+")").val('');
                }
            });


        })
    </script>
@endsection