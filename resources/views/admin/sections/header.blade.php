@extends('admin.partials.main')

@section('content')
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <h1>
                Edit Header Section
            </h1>
            <ol class="breadcrumb">
                <li><a href="#"><i class="fa fa-dashboard"></i> Dashboard</a></li>
                <li><a href="#">Section</a></li>
                <li class="active">Header</li>
            </ol>
        </section>

        <!-- Main content -->
        <section class="content">
            <div class="row">
                {!! Form::open(['action'=>['AdminSectionController@update',$section->id],'files'=>true,'method'=>'PUT']) !!}
                <div class="col-md-9">
                    <div class="box">
                        <!-- form start -->
                        <div class="box-body">
                            <div class="form-group">
                                <label for="email">Email Address</label>
                                <input type="text" name="contents[email]" value="@if(isset($section->contents['email'])){{ $section->contents['email'] }} @endif" placeholder="Email Address" id="email" class="form-control" required>
                            </div>
                            <label for="inputEmail3" class="col-sm-12 control-label ic_padding_left_null" style="padding-left: 0px">Pnone Numbers</label>
                            <div class="PhoneNumberGroup">
                                @if(isset($section->contents['phone_number']))
                                    @php($sl=0)
                                    @foreach($section->contents['phone_number'] as $phone_number)
                                        <div class="form-group eachPhoneNumber ic_clear ic_padding_top">
                                            <div class="col-sm-10 ic_padding_left_null" style="padding-left: 0px">
                                                <input name="contents[phone_number][{{ $sl }}]" value="{{ $phone_number }}" class="form-control input-sm" placeholder="Phone Number" type="text">
                                            </div>
                                            <div class="col-sm-2">
                                                <button type="button" class="btn btn-xs btn-danger" onclick="if (confirm('Are You Sure ?')){ $(this).parent().parent().remove();}"><i class="fa fa-trash-o"></i></button>
                                            </div>
                                        </div>
                                        @php($sl++)
                                    @endforeach
                                @endif
                            </div>
                            <div class="form-group col-sm-12 pull-left ic_padding_left_null" style="padding-top: 15px; padding-left: 0px">
                                <button type="button" id="addPhone" class="btn btn-xs btn-success"><i class="fa fa-plus-circle"></i> Add Phone</button>
                            </div>
                            <label for="inputEmail3" class="col-sm-12 control-label ic_padding_left_null" style="padding-left: 0px">Social Links</label>
                            <div class="SocialLinksGroup">
                                @if(isset($section->contents['social_links']))
                                    @php($sl=0)
                                    @foreach($section->contents['social_links'] as $social_link)
                                        <div class="form-group eachSocialLink ic_clear ic_padding_top">
                                            <div class="col-sm-3 ic_padding_left_null" style="padding-left: 0px">
                                                <input name="contents[social_links][{{ $sl }}][name]" value="{{ $social_link['name'] }}" class="form-control input-sm" placeholder="Social Media Name" type="text">
                                            </div>
                                            <div class="col-sm-4">
                                                <input name="contents[social_links][{{ $sl }}][url]" value="{{ $social_link['url'] }}" class="form-control input-sm" placeholder="Social Media URL" type="text">
                                            </div>
                                            <div class="col-sm-3">
                                                <input name="contents[social_links][{{ $sl }}][icon]" value="{{ $social_link['icon'] }}" class="form-control input-sm" placeholder="Font Awesome Icon Class" type="text">
                                            </div>
                                            <div class="col-sm-2">
                                                <button type="button" class="btn btn-xs btn-danger" onclick="if (confirm('Are You Sure ?')){ $(this).parent().parent().remove();}"><i class="fa fa-trash-o"></i></button>
                                            </div>
                                        </div>
                                        @php($sl++)
                                    @endforeach
                                @endif
                            </div>
                            <div class="form-group col-sm-12 pull-left ic_padding_left_null" style="padding-top: 15px; padding-left: 0px">
                                <button type="button" id="addSocialLink" class="btn btn-xs btn-success"><i class="fa fa-plus-circle"></i> Add New Social Link</button>
                            </div>
                        </div>
                        <div class="box-footer">
                            <button class="btn btn-primary" type="submit">Update</button>
                        </div>
                    </div>
                </div>
                <div class="col-md-3">
                    <div class="box">
                        <div class="box-header with-border">
                            <h3 class="box-title">Logo</h3>
                        </div>
                        <div class="box-body text-center">
                            <div class="fileinput fileinput-new" data-provides="fileinput">
                                <div class="fileinput-new thumbnail" style="max-width: 200px; max-height: 150px;">
                                    <img src="@if(isset($section->contents['logo']) && $section->contents['logo'] != ''){{ Storage::url($section->contents['logo']) }} @else{{ 'http://placehold.it/200x200' }} @endif" width="100%" alt="...">
                                </div>
                                <div class="fileinput-preview fileinput-exists thumbnail" style="max-width: 200px; max-height: 150px;"></div>
                                <div>
                            <span class="btn btn-default btn-file"><span class="fileinput-new">Select image</span><span class="fileinput-exists">Change</span>
                            <input type="file" name="logo">
                            </span>
                                    <a href="#" class="btn btn-default fileinput-exists" data-dismiss="fileinput">Remove</a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                {!! Form::close() !!}
            </div>
            <!-- ./row -->
        </section>
        <!-- /.content -->
    </div>
@endsection

@section('js')
    <script src="https://cdn.ckeditor.com/4.7.3/standard/ckeditor.js"></script>
    <script>
        CKEDITOR.replaceClass='editor';
    </script>
    <script>
        //    For Footer Service Section
        $('#addPhone').click(function () {
            var recentCounter = $('.eachPhoneNumber').length;

            var data='<div class="form-group eachPhoneNumber ic_clear ic_padding_top">\n' +
                '                                            <div class="col-sm-10 ic_padding_left_null" style="padding-left: 0px">\n' +
                '                                                <input name="contents[phone_number]['+recentCounter+']"  class="form-control input-sm" placeholder="Phone Number" type="text">\n' +
                '                                            </div>\n' +
                '                                            <div class="col-sm-2 pull-left">\n' +
                '                                                <button type="button" class="btn btn-xs btn-danger" onclick="if (confirm(\'Are You Sure ?\')){ $(this).parent().parent().remove();}"><i class="fa fa-trash-o"></i></button>\n' +
                '                                            </div>\n' +
                '                                        </div>';
            $('.PhoneNumberGroup').append(data);
        });

        //    For Social Links
        $('#addSocialLink').click(function () {
            var recentCounter = $('.eachSocialLink').length;

            var data='<div class="form-group eachSocialLink ic_clear ic_padding_top">\n' +
                '                                            <div class="col-sm-3 ic_padding_left_null" style="padding-left: 0px">\n' +
                '                                                <input name="contents[social_links]['+recentCounter+'][name]" class="form-control input-sm" placeholder="Social Media Name" type="text">\n' +
                '                                            </div>\n' +
                '                                            <div class="col-sm-4">\n' +
                '                                                <input name="contents[social_links]['+recentCounter+'][url]" class="form-control input-sm" placeholder="Social Media URL" type="text">\n' +
                '                                            </div>\n' +
                '                                            <div class="col-sm-3">\n' +
                '                                                <input name="contents[social_links]['+recentCounter+'][icon]" class="form-control input-sm" placeholder="Font Awesome Icon Class" type="text">\n' +
                '                                            </div>\n' +
                '                                            <div class="col-sm-2">\n' +
                '                                                <button type="button" class="btn btn-xs btn-danger" onclick="if (confirm(\'Are You Sure ?\')){ $(this).parent().parent().remove();}"><i class="fa fa-trash-o"></i></button>\n' +
                '                                            </div>\n' +
                '                                        </div>';
            $('.SocialLinksGroup').append(data);
        });
    </script>
@endsection