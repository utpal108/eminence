@extends('admin.partials.main')
@section('content')
    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <h1>
                Sections
                <small>All Sections</small>
            </h1>
            <ol class="breadcrumb">
                <li><i class="fa fa-dashboard"></i> <a href="#">Dashboard</a></li>
                <li class="active">Sections</li>
            </ol>
        </section>

        <!-- Main content -->
        <section class="content">
            <div class="row">
                <div class="col-xs-12">
                    <div class="box">
                    {{--<div class="box-header">--}}
                    {{--<div class="pull-left">--}}
                    {{--<a id="add-button" title="Add New Section" class="btn btn-success" href="{{ action('AdminSectionController@create') }}"><i class="fa fa-plus-circle"></i> Add New Section</a>--}}
                    {{--</div>--}}
                    {{--</div>--}}
                    <!-- /.box-header -->
                        <div class="box-body table-responsive">
                            <table class="table table-bordered table-condesed">
                                <thead>
                                <tr>
                                    <th class="text-center">SL</th>
                                    <th class="text-center">Section Name</th>
                                    <th class="text-center">Action</th>
                                </tr>
                                </thead>
                                <tbody>
                                @php($sl=(($sections->currentPage()-1)*$sections->perPage())+1)
                                @foreach($sections as $section)
                                    <tr class="text-center">
                                        <td>{{ $sl }}</td>
                                        <td>{{ $section->name }}</td>
                                        <td>
                                            <a title="Edit" class="btn btn-xs btn-success edit-row" href="{{ action('AdminSectionController@edit',['id'=>$section->id]) }}">
                                                Edit
                                            </a>&nbsp;
                                            <a title="Delete" class="btn btn-xs btn-danger delete-row" href="#">
                                                Delete
                                            </a>
                                        </td>
                                    </tr>
                                    @php($sl++)
                                @endforeach
                                </tbody>
                            </table>
                        </div>
                        <!-- /.box-body -->
                        <div class="box-footer clearfix">
                            {{ $sections->links() }}
                        </div>
                    </div>
                    <!-- /.box -->
                </div>
            </div>
            <!-- ./row -->
        </section>
        <!-- /.content -->
    </div>
    <!-- /.content-wrapper -->
@endsection