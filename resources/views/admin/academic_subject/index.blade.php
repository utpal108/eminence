@extends('admin.partials.main')
@section('content')
    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <h1>
                Academic Subjects
                <small>All Subjects</small>
            </h1>
            <ol class="breadcrumb">
                <li>
                    <i class="fa fa-dashboard"></i> <a href="#">Dashboard</a>
                </li>
                <li class="active">All Subjects</li>
            </ol>
        </section>

        <!-- Main content -->
        <section class="content">
            <div class="row">
                <div class="col-xs-12">
                    <div class="box">
                        @can('add_subject')
                            <div class="box-header">
                                <div class="pull-left">
                                    <a id="add-button" title="Add New Subject" class="btn btn-success" href="{{ action('SubjectController@create') }}"><i class="fa fa-plus-circle"></i> Add New Subject</a>
                                </div>
                            </div>
                        @endcan
                        <!-- /.box-header -->
                        <div class="box-body table-responsive all_subjects">
                            <table class="table table-bordered table-condesed">
                                <thead>
                                <tr>
                                    <th class="text-center">SL</th>
                                    <th class="text-center">Subject</th>
                                    <th class="text-center">Department</th>
                                    <th class="text-center">Group</th>
                                    <th class="text-center">Section</th>
                                    <th class="text-center">Action</th>
                                </tr>
                                </thead>
                                <tbody id="sortable">
                                    @php($sl=1)
                                    @foreach($all_subjects as $subject)
                                        <tr class="text-center ic-single-item ui-state-default" id="order_{{ $subject->id }}" >
                                            <td>{{ $sl }}</td>
                                            <td>{{ $subject->subject_name }}</td>
                                            <td>{{ $subject->department['name'] }}</td>
                                            <td>{{ $subject->group['group_name'] }}</td>
                                            <td>{{ $subject->section['section_name'] }}</td>
                                            <td>
                                                @can('edit_subject')
                                                    <a title="Edit" class="btn btn-xs btn-success edit-row" href="{{ action('SubjectController@edit',['id'=>$subject->id]) }}">
                                                        Edit
                                                    </a>&nbsp;
                                                @endcan
                                                @can('delete_subject')
                                                    {!! Form::open(['action'=>['SubjectController@destroy',$subject->id],'method'=>'delete', 'style'=>'display:inline']) !!}
                                                        <button title="Delete" class="btn btn-xs btn-danger delete-row" onclick="return confirm('Are You Sure ?')">
                                                            Delete
                                                        </button>
                                                    {!! Form::close() !!}
                                                @endcan
                                            </td>
                                        </tr>
                                        @php($sl++)
                                    @endforeach
                                </tbody>
                            </table>
                        </div>
                        <!-- /.box-body -->
                    </div>
                    <!-- /.box -->
                </div>
            </div>
            <!-- ./row -->
        </section>
        <!-- /.content -->
    </div>
    <!-- /.content-wrapper -->
@endsection

@section('js')
    <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
    <script src="{{ asset('ic_admin/js/jquery.toaster.js') }}"></script>
    <script>
        $( function() {
            $("#sortable").sortable({
                stop: function(){
                    var order = $(this).sortable('serialize');
                        $.ajax({
                        type: "POST",
                        dataType: "json",
                        url: "{{ url('admin/academic_subjects/order') }}",
                        data: {
                            orders: order,
                            _token: '{{csrf_token()}}'
                        }
                    });

                    $.toaster('Order Changed Successfully!', 'Status');

                }
            });
            $( "#sortable" ).disableSelection();
        } );
    </script>
@endsection