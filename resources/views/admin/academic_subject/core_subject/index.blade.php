@extends('admin.partials.main')
@section('content')
    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <h1>
                Main Subjects
                <small>All Main Subjects</small>
            </h1>
            <ol class="breadcrumb">
                <li>
                    <i class="fa fa-dashboard"></i> <a href="#">Dashboard</a>
                </li>
                <li class="active">All Main Subjects</li>
            </ol>
        </section>

        <!-- Main content -->
        <section class="content">
            <div class="row">
                <div class="col-xs-12">
                    <div class="box">
                        @can('add_main_subject')
                        <div class="box-header">
                            <div class="pull-left">
                                <a id="add-button" title="Add New Main Subjects" class="btn btn-success" href="{{ action('MainSubjectsController@create') }}"><i class="fa fa-plus-circle"></i> Add New Subject</a>
                            </div>
                        </div>
                        @endcan
                        <!-- /.box-header -->
                        <div class="box-body table-responsive">
                            <table class="table table-bordered table-condesed">
                                <thead>
                                <tr>
                                    <th class="text-center">SL</th>
                                    <th class="text-center">Subject Name</th>
                                    <th class="text-center">About Exam</th>
                                    <th class="text-center">Action</th>
                                </tr>
                                </thead>
                                <tbody>
                                @php($sl=(($main_subjects->currentPage()-1)*$main_subjects->perPage())+1)
                                @foreach($main_subjects as $main_subject)
                                    <tr class="text-center">
                                        <td>{{ $sl }}</td>
                                        <td>{{ $main_subject->name }}</td>
                                        <td>{!! $main_subject->details !!} </td>
                                        <td>
                                            @can('edit_main_subject')
                                                <a title="Edit" class="btn btn-xs btn-success edit-row" href="{{ action('MainSubjectsController@edit',['id'=>$main_subject->id]) }}">
                                                    Edit
                                                </a>&nbsp;
                                            @endcan
                                            @can('delete_main_subject')
                                                {!! Form::open(['action'=>['MainSubjectsController@destroy',$main_subject->id],'method'=>'delete', 'style'=>'display:inline']) !!}
                                                    <button title="Delete" class="btn btn-xs btn-danger delete-row" onclick="return confirm('Are You Sure ?')">
                                                        Delete
                                                    </button>
                                                {!! Form::close() !!}
                                            @endcan
                                        </td>
                                    </tr>
                                    @php($sl++)
                                @endforeach
                                </tbody>
                            </table>
                        </div>
                        <!-- /.box-body -->
                        <div class="box-footer clearfix">
                            {{ $main_subjects->links() }}
                        </div>
                    </div>
                    <!-- /.box -->
                </div>
            </div>
            <!-- ./row -->
        </section>
        <!-- /.content -->
    </div>
    <!-- /.content-wrapper -->
@endsection