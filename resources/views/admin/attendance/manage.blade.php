@extends('admin.partials.main')

@section('style')
    <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
@endsection
@section('content')
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <h1>
                Daily Attendance
            </h1>
            <ol class="breadcrumb">
                <li><a href="#"><i class="fa fa-dashboard"></i> Dashboard</a></li>
                <li><a href="#">Attendance</a></li>
                <li class="active">Daily Attendance</li>
            </ol>
        </section>

        <!-- Main content -->
        <section class="content">
            <div class="row">
                <div class="col-xs-12">
                    <div class="box">
                        <!-- form start -->
                        {!! Form::open(['action'=>'StudentAttendanceController@manage','method'=>'GET']) !!}
                            <div class="box-body">
                            <div class="form-group col-md-2">
                                <label>Department</label>
                                <select name="department" class="form-control" id="department_id" required>
                                    @if(isset($departments) && $departments != '')
                                        @foreach($departments as $department)
                                            <option value="{{ $department->id }}" @if($department->id == $input_data['department']){{ 'selected' }} @endif >{{ $department->name }}</option>
                                        @endforeach
                                    @endif
                                </select>
                            </div>
                            <div class="form-group col-md-2">
                                <label>Group</label>
                                <select name="group" class="form-control" id="group_id" required>
                                    @if(isset($groups) && $groups != '')
                                        @foreach($groups as $group)
                                            <option value="{{ $group->id }}" @if($group->id == $input_data['group']){{ 'selected' }} @endif >{{ $group->group_name }}</option>
                                        @endforeach
                                    @endif
                                </select>
                            </div>
                                <div class="form-group col-md-2">
                                    <label>Section</label>
                                    <select name="section" class="form-control" id="section_id" required>
                                        @if(isset($sections) && $sections != '')
                                            @foreach($sections as $section)
                                                <option value="{{ $section->id }}" @if($section->id == $input_data['section']){{ 'selected' }} @endif >{{ $section->section_name }}</option>
                                            @endforeach
                                        @endif
                                    </select>
                                </div>
                                <div class="form-group col-md-2">
                                    <label>Session</label>
                                    <select name="student_session" class="form-control" required>
                                        @if(isset($sessions) && $sessions != '')
                                            @foreach($sessions as $session)
                                                <option value="{{ $session->id }}" @if($session->id == $input_data['student_session']){{ 'selected' }} @endif>{{ $session->session_name }}</option>
                                            @endforeach
                                        @endif
                                    </select>
                                </div>
                            <div class="form-group col-md-2">
                                <label>Subject</label>
                                <select name="subject_id" class="form-control" id="subject_id" required>
                                    @if(isset($subjects) && $subjects != '')
                                        @foreach($subjects as $subject)
                                            <option value="{{ $subject->id }}" @if($subject->id == $input_data['subject_id']){{ 'selected' }} @endif >{{ $subject->subject_name }}</option>
                                        @endforeach
                                    @endif
                                </select>
                            </div>
                            <div class="form-group col-md-2">
                                <label for="datepicker">Date</label>
                                <input name="date" type="text" id="datepicker" value="{{ $input_data['date'] }}" class="form-control" required>
                            </div>
                            <div class="form-group col-md-2">
                                <button class="btn btn-primary" style="margin-top: 24px" type="submit">Manage Attendance</button>
                            </div>
                        </div>
                            <!-- /.box-body -->
                        {!! Form::close() !!}
                    </div>
                    <div class="box">
                        {{--<div class="box-header">--}}
                            {{--<p>Box Header</p>--}}
                        {{--</div>--}}
                        <div class="box-body">
                            <table id="example2" class="table table-bordered table-hover dataTable" role="grid" aria-describedby="example2_info">
                                <thead>
                                <tr role="row">
                                    <th>#</th>
                                    <th>Student ID</th>
                                    <th>Name</th>
                                    <th>Status</th>
                                </tr>
                                </thead>
                                <tbody>
                                {!! Form::open(['action'=>'StudentAttendanceController@store']) !!}
                                    @if(isset($students) && $students != "")
                                        <input type="hidden" name="department" value="{{ $input_data['department'] }}">
                                        <input type="hidden" name="group" value="{{ $input_data['group'] }}">
                                        <input type="hidden" name="subject_id" value="{{ $input_data['subject_id'] }}">
                                        <input type="hidden" name="session" value="{{ $input_data['student_session'] }}">
                                        <input type="hidden" name="section" value="{{ $input_data['section'] }}">
                                        <input type="hidden" name="date" value="{{ $input_data['date'] }}">
                                        @php($sl=1)
                                        @foreach($students as $student)
                                            <tr role="row" class="@if($sl%2==0){{ 'even' }} @else{{ 'odd' }} @endif">
                                                <input type="hidden" class="student_id" name="student_id[]" value="{{ $student->id }}">
                                                <td class="">{{ $sl }}</td>
                                                <td class="">{{ $student->student_id }}</td>
                                                <td class="">{{ $student->name }}&nbsp;&nbsp;<button class="show_details" type="button">Details</button></td>
                                                {{--<td class="col-md-4">--}}
                                                    {{--{!! Form::select('attendance_status[]', ['P' => 'Present', 'A' => 'Absent'], $student->specific_attendance['status'], ['class ' => 'form-control select2 select2-hidden-accessible status']); !!}--}}
                                                {{--</td>--}}
                                                <td style="padding: 0px 0px 0px 15px">
                                                    <div class="form-group radio">
                                                        <label>
                                                            <input type="radio" name="attendance_status[{{ $sl-1 }}]" id="optionsRadios1" value="P" @if($student->specific_attendance['status']=='P' || $student->specific_attendance['status'] ==''){{ 'checked' }} @endif >Present &nbsp;
                                                        </label>
                                                        <label>
                                                            <input type="radio" name="attendance_status[{{ $sl-1 }}]" id="optionsRadios2" value="A" @if($student->specific_attendance['status']=='A'){{ 'checked' }} @endif>Absent &nbsp;
                                                        </label>
                                                        <label>
                                                            <input type="radio" name="attendance_status[{{ $sl-1 }}]" id="optionsRadios3" value="L" @if($student->specific_attendance['status']=='L'){{ 'checked' }} @endif >Late &nbsp;
                                                        </label>
                                                    </div>
                                                </td>
                                            </tr>
                                            <tr class="details_info" style="display: none">
                                                <td><img src="{{ Storage::url($student->student_image) }}" class="img-responsive img-thumbnail" style="max-height: 70px"></td>
                                                <td colspan="3">
                                                    <p class="ic_margin_top5">Name: {{ $student->name }}</p>
                                                    <p class="ic_margin_top5">ID: {{ $student->student_id }}</p>
                                                    <p class="ic_margin_top5">Gurdian's Name: {{ $student->student_info['gurdian_name'] }}</p>
                                                    <p class="ic_margin_top5">Gurdian's Contact: {{ $student->student_info['gurdian_contact'] }}</p>
                                                    <p class="ic_margin_top5">Present Address: {!! $student->student_info['present_address'] !!}</p>
                                                    <p class="ic_margin_top5">Write Message To Gurdian:</p>
{{--                                                    {!! Form::open(['action'=>'StudentAttendanceController@index']) !!}--}}
                                                    <input type="hidden" name="sms_number" value="{{ $student->student_info['sms_number'] }}" class="sms_number">
                                                    <textarea placeholder="Write Message ..." rows="2" cols="25" class="teacher_message"></textarea><br>
                                                    <button class="btn btn-success btn-sm send_message" type="button">Send Message</button>
{{--                                                    {!! Form::close() !!}--}}
                                                </td>
                                            </tr>
                                            @php($sl++)
                                        @endforeach
                                    @endif
                                </tbody>
                            </table>
                        </div>
                        @can('student_attendance')
                            <div class="box-footer">
                                <button class="btn btn-primary" id="save_change" type="submit">Save Change</button>
                            </div>
                        @endcan
                        {!! Form::close() !!}
                    </div>
                </div>
            </div>
            <!-- ./row -->
        </section>
        <!-- /.content -->
    </div>
@endsection
@section('js')
    <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
    <script src="{{ asset('ic_admin/js/jquery.toaster.js') }}"></script>
    <!-- iCheck 1.0.1 -->
    <script src="{{ asset('ic_admin/plugins/iCheck/icheck.min.js') }}"></script>
    <script>
        //iCheck for checkbox and radio inputs
        $('input[type="checkbox"].minimal, input[type="radio"].minimal').iCheck({
            checkboxClass: 'icheckbox_minimal-blue',
            radioClass   : 'iradio_minimal-blue'
        })
        //Red color scheme for iCheck
        $('input[type="checkbox"].minimal-red, input[type="radio"].minimal-red').iCheck({
            checkboxClass: 'icheckbox_minimal-red',
            radioClass   : 'iradio_minimal-red'
        })
        //Flat red color scheme for iCheck
        $('input[type="checkbox"].flat-red, input[type="radio"].flat-red').iCheck({
            checkboxClass: 'icheckbox_flat-green',
            radioClass   : 'iradio_flat-green'
        })
    </script>
    <script>
        $( function() {
            $( "#datepicker" ).datepicker({ dateFormat: 'yy-mm-dd' });
        } );

        $('.show_details').click(function () {
            $('.details_info').hide();
            $(this).parent().parent().next('.details_info').show(1000);
        });

        $('.send_message').click(function (e) {
            var sms_number=$(this).prevAll("input.sms_number:first").val();
            var teacher_message=$(this).prevAll("textarea.teacher_message:first").val();
            $.ajax({
                type:'GET',
                url:'/ajax_call/send_message',
                data:{
                    'sms_number':sms_number,
                    'teacher_message':teacher_message
                },
                success:function (response) {
                    var res =JSON.parse(response);
                    if (res.status == 'success'){
                        $.toaster('Message Send Successfully','Status','success');
                    }
                    else {
                        $.toaster('Message Send Failed','Status','danger');
                    }
                }
            })
        });
    </script>
    <script>
        $(document).ready(function () {
            $('#department_id').change(function (e) {
                $('#subject_id').html('');
                $('#section_id').html('');
                $('#group_id').html('');
                var department=$('#department_id').val();
                $.ajax({
                    type:'GET',
                    url:'/ajax_call/department_wise_data',
                    data:{
                        'department_id':department
                    },
                    success:function (response) {
                        var res=JSON.parse(response);
                        $('#group_id').html(res.groups);
                    }
                })
            });

            $('#group_id').change(function (e) {
                $('#subject_id').html('');
                $('#section_id').html('');

                var department=$('#department_id').val();
                var group_id=$('#group_id').val();

                $.ajax({
                    type:'GET',
                    url:'/admin/group_wise_data',
                    data:{
                        department_id:department,
                        group_id:group_id,
                    },
                    success:function (response) {
                        var res=JSON.parse(response);
                        $('#section_id').html(res.sections);
                    }
                })
            });

            $('#section_id').change(function (e) {
                $('#subject_id').html('');

                var department=$('#department_id').val();
                var group_id=$('#group_id').val();
                var section_id=$('#section_id').val();

                $.ajax({
                    type:'GET',
                    url:'/admin/section_wise_data',
                    data:{
                        department_id:department,
                        group_id:group_id,
                        'section_id':section_id,
                    },
                    success:function (response) {
                        console.log(response);
                        var res=JSON.parse(response);
                        $('#subject_id').html(res.subjects);
                    }
                })
            })
        })
    </script>
@endsection