@extends('admin.partials.main')

@section('style')
    <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
@endsection
@section('content')
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <h1>
                Attendance Report
            </h1>
            <ol class="breadcrumb">
                <li><a href="#"><i class="fa fa-dashboard"></i> Dashboard</a></li>
                <li><a href="#">Attendance</a></li>
                <li class="active">Attendance Report</li>
            </ol>
        </section>

        <!-- Main content -->
        <section class="content">
            <div class="row">
                <div class="col-xs-12">
                    <div class="box">
                        <!-- form start -->
                        {!! Form::open(['action'=>'StudentAttendanceController@show_report','method'=>'GET']) !!}
                            {{ csrf_field() }}
                            <div class="box-body">
                                <div class="form-group col-md-2">
                                    <label>Department</label>
                                    <select name="department" class="form-control" id="department_id" required>
                                        <option value="">-- Select --</option>
                                            @if(isset($departments) && $departments != '')
                                                @foreach($departments as $department)
                                                    <option value="{{ $department->id }}">{{ $department->name }}</option>
                                                @endforeach
                                            @endif
                                    </select>
                                </div>
                                <div class="form-group col-md-2">
                                    <label>Group</label>
                                    <select name="group" id="group_id" class="form-control" required></select>
                                </div>
                                <div class="form-group col-md-2">
                                    <label>Section</label>
                                    <select name="section" id="section_id" class="form-control" required></select>
                                </div>
                                <div class="form-group col-md-2">
                                    <label>Session</label>
                                    <select name="student_session" class="form-control" required>
                                        @if(isset($sessions) && $sessions != '')
                                            @foreach($sessions as $session)
                                                <option value="{{ $session->id }}">{{ $session->session_name }}</option>
                                            @endforeach
                                        @endif
                                    </select>
                                </div>
                                <div class="form-group col-md-2">
                                    <label>Subject</label>
                                    <select name="subject_id" class="form-control" id="subject_id" required></select>
                                </div>
                                <div class="form-group col-md-2">
                                    <label for="datepicker">Year</label>
                                    <select name="year" class="form-control" required>
                                        @for($i=2018;$i<2050;$i++)
                                            <option value="{{ $i }}">{{ $i }}</option>
                                        @endfor
                                    </select>
                                </div>
                                <div class="form-group col-md-2">
                                    <label for="datepicker">Monthe</label>
                                    <select name="month" class="form-control" required>
                                        <option value="01">Jan</option>
                                        <option value="02">Feb</option>
                                        <option value="03">Mar</option>
                                        <option value="04">April</option>
                                        <option value="05">May</option>
                                        <option value="06">June</option>
                                        <option value="07">July</option>
                                        <option value="08">August</option>
                                        <option value="09">September</option>
                                        <option value="10">October</option>
                                        <option value="11">November</option>
                                        <option value="12">December</option>
                                    </select>
                                </div>
                                @can('attendance_report')
                                    <div class="form-group col-md-1">
                                        <button class="btn btn-primary" style="margin-top: 24px" type="submit">Report</button>
                                    </div>
                                @endcan
                            </div>
                            <!-- /.box-body -->
                        {!! Form::close() !!}
                    </div>
                </div>
            </div>
            <!-- ./row -->
        </section>
        <!-- /.content -->
    </div>
@endsection
@section('js')
    <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
    <script>
        $( function() {
            $( "#datepicker" ).datepicker({ dateFormat: 'yy-mm-dd' });
        } );
    </script>
    <script>
        $(document).ready(function () {
            $('#department_id').change(function (e) {
                $('#subject_id').html('');
                $('#section_id').html('');
                $('#group_id').html('');
                var department=$('#department_id').val();
                $.ajax({
                    type:'GET',
                    url:'/ajax_call/department_wise_data',
                    data:{
                        'department_id':department
                    },
                    success:function (response) {
                        var res=JSON.parse(response);
                        $('#group_id').html(res.groups);
                    }
                })
            });

            $('#group_id').change(function (e) {
                $('#subject_id').html('');
                $('#section_id').html('');

                var department=$('#department_id').val();
                var group_id=$('#group_id').val();

                $.ajax({
                    type:'GET',
                    url:'/admin/group_wise_data',
                    data:{
                        department_id:department,
                        group_id:group_id,
                    },
                    success:function (response) {
                        var res=JSON.parse(response);
                        $('#section_id').html(res.sections);
                    }
                })
            });

            $('#section_id').change(function (e) {
                $('#subject_id').html('');

                var department=$('#department_id').val();
                var group_id=$('#group_id').val();
                var section_id=$('#section_id').val();

                $.ajax({
                    type:'GET',
                    url:'/admin/section_wise_data',
                    data:{
                        department_id:department,
                        group_id:group_id,
                        'section_id':section_id,
                    },
                    success:function (response) {
                        console.log(response);
                        var res=JSON.parse(response);
                        $('#subject_id').html(res.subjects);
                    }
                })
            })
        })
    </script>
@endsection