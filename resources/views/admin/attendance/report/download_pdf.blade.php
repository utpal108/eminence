<style>
    table, th, td {
        border: 1px solid black;
        border-collapse: collapse;
    }
    th, td {
        padding: 5px;
        text-align: left;
    }
    .align-middle{
        text-align: center;
    }
    @page { margin: 50px 25px 100px 25px; }
    /*header { position: fixed; top: -60px; left: 0px; right: 0px; background-color: lightblue; height: 50px; }*/
    footer { position: fixed; bottom: -60px; left: 0px; right: 0px; height: 50px; }
    p { page-break-after: always; }
    p:last-child { page-break-after: never; }
</style>
<div class="ic-pdf" style="text-align: center; height: 150px;">
    <div style="width: 40%; float: left; text-align: right; ">
        <img src="{{ $_SERVER['DOCUMENT_ROOT'].'/images/eminence_college_logo.png' }}" style="margin-right: 60px; display: inline-block; width: 120px; height: 120px">
    </div>
    <div style="width: 60%; float: right; text-align: left; margin-top: 20px;">
        <h2>
            Student Attendance Report<br>
            <span style="font-size: 15px; margin-left: 30px; ; margin-top: 5px; display: block">Month : {{ $month }} &nbsp;&nbsp;&nbsp; Subject : {{ $subject->subject_name }}</span>
            <span style="font-size: 15px;  margin-left: 30px; display: block; margin-top: 5px">Group : {{ $group->group_name }} &nbsp;&nbsp;&nbsp; Session : {{ $session->session_name }}</span>
            <span style="font-size: 15px;  margin-left: 30px; display: block; margin-top: 5px">Year : {{ date('Y') }}</span>
        </h2>
    </div>
</div>
<footer style="text-align: right">
    <br>
    <span>-------------------------------</span><br>
    <span>Course Teacher Signature</span>
</footer>

<table style="width: 100%; font-size: 10px">
    <thead>
    <tr role="row">
        <th colspan="2" class="align-middle">Student | Date</th>
        <th class="align-middle">1</th>
        <th class="align-middle">2</th>
        <th class="align-middle">3</th>
        <th class="align-middle">4</th>
        <th class="align-middle">5</th>
        <th class="align-middle">6</th>
        <th class="align-middle">7</th>
        <th class="align-middle">8</th>
        <th class="align-middle">9</th>
        <th class="align-middle">10</th>
        <th class="align-middle">11</th>
        <th class="align-middle">12</th>
        <th class="align-middle">13</th>
        <th class="align-middle">14</th>
        <th class="align-middle">15</th>
        <th class="align-middle">16</th>
        <th class="align-middle">17</th>
        <th class="align-middle">18</th>
        <th class="align-middle">19</th>
        <th class="align-middle">20</th>
        <th class="align-middle">21</th>
        <th class="align-middle">22</th>
        <th class="align-middle">23</th>
        <th class="align-middle">24</th>
        <th class="align-middle">25</th>
        <th class="align-middle">26</th>
        <th class="align-middle">27</th>
        <th class="align-middle">28</th>
        <th class="align-middle">29</th>
        <th class="align-middle">30</th>
        <th class="align-middle">31</th>
        <th class="align-middle">P</th>
        <th class="align-middle">L</th>
        <th class="align-middle">A</th>
        <th class="align-middle">P%</th>
    </tr>
    </thead>
    <tbody>
    @php($sl=1)
    @foreach($attendances as $attendance)
        <?php
        $att_data=$attendance->attendance->toArray();
        $att_data=array_column($att_data,'status','date');
        $count_attendance=array_count_values($att_data);
        if (isset($count_attendance['P'])){
            $total_present=$count_attendance['P'];
        }
        else{
            $total_present=0;
        }

        if (isset($count_attendance['A'])){
            $total_absent=$count_attendance['A'];
        }
        else{
            $total_absent=0;
        }

        if (isset($count_attendance['L'])){
            $total_late=$count_attendance['L'];
        }
        else{
            $total_late=0;
        }

        if (count($att_data) >0){
            $present_percent=round(($total_present/count($att_data))*100);
        }
        else{
            $present_percent=0;
        }
        ?>
        <tr role="row" class="@if($sl%2 == 0){{ 'even' }} @else{{ 'odd' }} @endif">
            <td class="align-middle">{{ $attendance['name'] }}</td>
            <td class="align-middle">{{ $attendance['student_id'] }}</td>
            <td class="align-middle">@if(isset($att_data[$date_from .'-01'])){{ $att_data[$date_from .'-01'] }} @else {{ '-' }} @endif </td>
            <td class="align-middle">@if(isset($att_data[$date_from .'-02'])){{ $att_data[$date_from .'-02'] }} @else {{ '-' }} @endif </td>
            <td class="align-middle">@if(isset($att_data[$date_from .'-03'])){{ $att_data[$date_from .'-03'] }} @else {{ '-' }} @endif </td>
            <td class="align-middle">@if(isset($att_data[$date_from .'-04'])){{ $att_data[$date_from .'-04'] }} @else {{ '-' }} @endif </td>
            <td class="align-middle">@if(isset($att_data[$date_from .'-05'])){{ $att_data[$date_from .'-05'] }} @else {{ '-' }} @endif </td>
            <td class="align-middle">@if(isset($att_data[$date_from .'-06'])){{ $att_data[$date_from .'-06'] }} @else {{ '-' }} @endif </td>
            <td class="align-middle">@if(isset($att_data[$date_from .'-07'])){{ $att_data[$date_from .'-07'] }} @else {{ '-' }} @endif </td>
            <td class="align-middle">@if(isset($att_data[$date_from .'-08'])){{ $att_data[$date_from .'-08'] }} @else {{ '-' }} @endif </td>
            <td class="align-middle">@if(isset($att_data[$date_from .'-09'])){{ $att_data[$date_from .'-09'] }} @else {{ '-' }} @endif </td>
            <td class="align-middle">@if(isset($att_data[$date_from .'-10'])){{ $att_data[$date_from .'-10'] }} @else {{ '-' }} @endif </td>
            <td class="align-middle">@if(isset($att_data[$date_from .'-11'])){{ $att_data[$date_from .'-11'] }} @else {{ '-' }} @endif </td>
            <td class="align-middle">@if(isset($att_data[$date_from .'-12'])){{ $att_data[$date_from .'-12'] }} @else {{ '-' }} @endif </td>
            <td class="align-middle">@if(isset($att_data[$date_from .'-13'])){{ $att_data[$date_from .'-13'] }} @else {{ '-' }} @endif </td>
            <td class="align-middle">@if(isset($att_data[$date_from .'-14'])){{ $att_data[$date_from .'-14'] }} @else {{ '-' }} @endif </td>
            <td class="align-middle">@if(isset($att_data[$date_from .'-15'])){{ $att_data[$date_from .'-15'] }} @else {{ '-' }} @endif </td>
            <td class="align-middle">@if(isset($att_data[$date_from .'-16'])){{ $att_data[$date_from .'-16'] }} @else {{ '-' }} @endif </td>
            <td class="align-middle">@if(isset($att_data[$date_from .'-17'])){{ $att_data[$date_from .'-17'] }} @else {{ '-' }} @endif </td>
            <td class="align-middle">@if(isset($att_data[$date_from .'-18'])){{ $att_data[$date_from .'-18'] }} @else {{ '-' }} @endif </td>
            <td class="align-middle">@if(isset($att_data[$date_from .'-19'])){{ $att_data[$date_from .'-19'] }} @else {{ '-' }} @endif </td>
            <td class="align-middle">@if(isset($att_data[$date_from .'-20'])){{ $att_data[$date_from .'-20'] }} @else {{ '-' }} @endif </td>
            <td class="align-middle">@if(isset($att_data[$date_from .'-21'])){{ $att_data[$date_from .'-21'] }} @else {{ '-' }} @endif </td>
            <td class="align-middle">@if(isset($att_data[$date_from .'-22'])){{ $att_data[$date_from .'-22'] }} @else {{ '-' }} @endif </td>
            <td class="align-middle">@if(isset($att_data[$date_from .'-23'])){{ $att_data[$date_from .'-23'] }} @else {{ '-' }} @endif </td>
            <td class="align-middle">@if(isset($att_data[$date_from .'-24'])){{ $att_data[$date_from .'-24'] }} @else {{ '-' }} @endif </td>
            <td class="align-middle">@if(isset($att_data[$date_from .'-25'])){{ $att_data[$date_from .'-25'] }} @else {{ '-' }} @endif </td>
            <td class="align-middle">@if(isset($att_data[$date_from .'-26'])){{ $att_data[$date_from .'-26'] }} @else {{ '-' }} @endif </td>
            <td class="align-middle">@if(isset($att_data[$date_from .'-27'])){{ $att_data[$date_from .'-27'] }} @else {{ '-' }} @endif </td>
            <td class="align-middle">@if(isset($att_data[$date_from .'-28'])){{ $att_data[$date_from .'-28'] }} @else {{ '-' }} @endif </td>
            <td class="align-middle">@if(isset($att_data[$date_from .'-29'])){{ $att_data[$date_from .'-29'] }} @else {{ '-' }} @endif </td>
            <td class="align-middle">@if(isset($att_data[$date_from .'-30'])){{ $att_data[$date_from .'-30'] }} @else {{ '-' }} @endif </td>
            <td class="align-middle">@if(isset($att_data[$date_from .'-31'])){{ $att_data[$date_from .'-31'] }} @else {{ '-' }} @endif </td>
            <td class="align-middle">{{ $total_present }}</td>
            <td class="align-middle">{{ $total_late }}</td>
            <td class="align-middle">{{ $total_absent }}</td>
            <td class="align-middle">{{ $present_percent .'%' }}</td>
        </tr>
        @php($sl++)
    @endforeach
    </tbody>
</table>