
(function(){
     "use strict";
	jQuery(document).ready(function(){
		//Mobile nav
	    jQuery(function(){
		    $('#menu').slicknav({
		        label: ""
		    });
	    });
	    // Home slider
		$('.ic-slider').slick({
		  dots: true,
		  arrows: false,
		  infinite: true,
		  speed: 300,
		  slidesToShow: 1,
		});
	    // talbe custom scrollbar

        // if($(window).width() < 768) {
	       //  $(window).on("load",function(){
	       //      $(".ic-narrow-screen-tbl").mCustomScrollbar({
			     //  axis:"x",
			     //  theme:"dark-3"
	       //      });
	       //  });
        // }
        $(window).on("load",function(){
            $(".ic-narrow-screen-tbl").mCustomScrollbar({
		      axis:"x",
		      theme:"dark-3"
            });
        });


        // gallery mixitup initialize
        var icProperty = document.querySelector('.ic-portfolio-items');
        if(icProperty){
          var mixer = mixitup(icProperty, {
                animation: {
                   effects: 'fade translateZ(-100px)',
                },
          });
        }

        // Lightbox initialize
		$("[data-fancybox]").fancybox({
			// Options will go here
		});

	});
})();